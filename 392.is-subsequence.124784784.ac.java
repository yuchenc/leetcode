class Solution {
    public boolean isSubsequence(String s, String t) {
        int left = 0;
        int right = 0;
        while (left < s.length() && right < t.length())
        {
            if (t.charAt(right) == s.charAt(left))
            {
                left++;
                right++;
            }
            else
                right++;
        }
        return left == s.length();
    }
}