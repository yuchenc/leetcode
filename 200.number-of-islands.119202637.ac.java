class Solution {
    public int numIslands(char[][] grid) {
        if (grid.length == 0)
            return 0;
        int m = grid.length;
        int n = grid[0].length;
        int count = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] == '1')
                {
                    count++;
                    dig(grid, i, j);
                }
            }
        }
        return count;
    }
    
    public void dig(char[][] grid, int i, int j)
    {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid[0].length || grid[i][j] == '0')
            return;
        grid[i][j] = '0';
        dig(grid, i - 1, j);
        dig(grid, i + 1, j);
        dig(grid, i, j - 1);
        dig(grid, i, j + 1);
    }
}