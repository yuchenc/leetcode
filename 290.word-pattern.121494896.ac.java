class Solution {
    public boolean wordPattern(String pattern, String str) {
        String[] strs = str.split(" ");
        if (strs.length != pattern.length())
            return false;
        HashMap<Character, String> map = new HashMap<>();
        HashMap<String, Character> map2 = new HashMap<>();
        for (int i = 0; i < strs.length; i++)
        {
            char c = pattern.charAt(i);
            String words = strs[i];
            if (map.containsKey(c) && map2.containsKey(words))
            {
                if (!words.equals(map.get(c)) || map2.get(words) != c)
                    return false;
            }
            else if (!map.containsKey(c) && !map2.containsKey(words))
            {
                map.put(c, words);
                map2.put(words, c);
            }
            else
                return false;
        }
        return true;
    }
}