/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode insertionSortList(ListNode head) {
        if (head == null)
            return null;
        ListNode headhead = new ListNode(0);
        headhead.next = head;
        ListNode current = head.next;
        head.next = null;
        while (current != null)
        {
            ListNode search = headhead.next;
            ListNode tmp = current.next;
            ListNode prev = headhead;
            while (search != null && search.val < current.val)
            {
                search = search.next;
                prev = prev.next;
            }
            prev.next = current;
            current.next = search;
            current = tmp;
        }
        return headhead.next;
    }
}