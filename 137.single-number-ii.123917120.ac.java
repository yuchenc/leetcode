class Solution {
    public int singleNumber(int[] nums) {
        int a = 0;
        int b = 0;
        for (int c : nums)
        {
            int tmp = a;
            a = (~a&b&c) | (a&~b&~c);
            b = (~tmp&~b&c) | (~tmp&b&~c);
        }
        return a | b;
    }
}