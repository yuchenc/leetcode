class Solution {
    public int[] exclusiveTime(int n, List<String> logs) {
        int[] answer = new int[n];
        Stack<Integer> stack = new Stack<>();
        int prev = 0;
        for (int i = 0; i < logs.size(); i++)
        {
            String str = logs.get(i);
            String[] parts = str.split(":");
            int id = Integer.parseInt(parts[0]);
            int time = Integer.parseInt(parts[2]);
            String op = str.split(":")[1];
            if (!stack.isEmpty())
                answer[stack.peek()] += time - prev;
            prev = time;
            if (op.equals("start"))
                stack.push(id);
            if (op.equals("end"))
            {
                answer[stack.pop()]++;
                prev++;
            }
        }
        return answer;
    }
}