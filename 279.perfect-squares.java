class Solution {
    public int numSquares(int n) {
        if (is_square(n))
            return 1;
        while ((n & 3) == 0)
            n = n >> 2;
        if ((n & 7) == 7)
            return 4;
        int sqrt = (int) Math.sqrt(n);
        for (int i = 1; i <= sqrt; i++)
        {
            if (is_square(n - i*i))
                return 2;
        }
        return 3;
    }
    public boolean is_square(int n)
    {
        int a = (int) Math.sqrt(n);
        if (n == a*a)
            return true;
        else
            return false;
    }
}
