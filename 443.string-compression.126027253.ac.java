class Solution {
    public int compress(char[] chars) {
        int count = 0;
        int index = 0;
        for (int i = 0; i < chars.length; i++)
        {
            if (i == chars.length-1 || chars[i] != chars[i+1])
            {
                if (count == 0)
                {
                    chars[index] = chars[i];
                    index++;
                }
                else
                {
                    chars[index] = chars[i];
                    index++;
                    String number = String.valueOf(count+1);
                    for (int j = 0; j < number.length(); j++)
                    {
                        chars[index] = number.charAt(j);
                        index++;
                    }
                    count = 0;
                }
            }
            else
                count++;
        }
        return index;
        
    }
}