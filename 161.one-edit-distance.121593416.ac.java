class Solution {
    public boolean isOneEditDistance(String s, String t) {
        if (s.length() == t.length())
        {
            int flag = 0;
            for (int i = 0; i < s.length(); i++)
            {
                if (s.charAt(i) != t.charAt(i))
                {
                    if (flag == 1)
                        return false;
                    flag = 1;
                }
            }
            if (flag == 0)
                return false;
            return true;
        }
        else if (s.length() == t.length() -1)
            return check(s, t);
        else if (s.length() == t.length() + 1)
            return check(t, s);
        else
            return false;
    }
    
    public boolean check(String s, String t) {
        int a = 0;
        int b = 0;
        int flag = 0;
        while (a < s.length() && b < t.length())
        {
            if (s.charAt(a) == t.charAt(b))
            {
                a++;
                b++;
            }
            else
            {
                if (flag == 1)
                    return false;
                flag = 1;
                b++;
            }
        }
        return true;
    }
}