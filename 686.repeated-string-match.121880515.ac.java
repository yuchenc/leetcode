class Solution {
    public int repeatedStringMatch(String A, String B) {
        int repeat = 0;
        String current = "";
        while (current.length() < B.length())
        {
            repeat++;
            current += A;
        }
        if (current.indexOf(B) >= 0)
            return repeat;
        current += A;
         if (current.indexOf(B) >= 0)
             return repeat + 1;
        return -1;
        
    }
}