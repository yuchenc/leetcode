class Solution {
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> answer = new ArrayList<>();
        for (int i = 2; i <= Math.sqrt(n); i++)
        {
            if (n%i == 0)
            {
                List<Integer> tmp = new ArrayList<>();
                tmp.add(i);
                tmp.add(n/i);
                answer.add(tmp);
                List<List<Integer>> former = getFactors(n/i);
                for (int j = 0; j < former.size(); j++)
                {
                    if (former.get(former.size() - j - 1).get(0) < i)
                        break;
                    else
                    {
                        tmp = new ArrayList<>();
                        tmp.add(i);
                        tmp.addAll(former.get(former.size() - j - 1));
                        answer.add(tmp);
                    }
                }
            }
        }
        return answer;
    }
}