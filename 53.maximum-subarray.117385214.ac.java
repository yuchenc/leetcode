class Solution {
    public int maxSubArray(int[] nums) {
        int max = nums[0];
        int maxposition = nums[0];
        for (int i = 1; i < nums.length; i++)
        {
            if (maxposition < 0)
                maxposition = nums[i];
            else
                maxposition += nums[i];
            if (maxposition > max)
                max = maxposition;
        }
        return max;
    }
}