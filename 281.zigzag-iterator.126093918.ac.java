public class ZigzagIterator {
    List<Integer> list = new ArrayList<>();
    int index;

    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        if (v1.size() >= v2.size())
        {
            for (int i = 0; i < v2.size(); i++)
            {
                list.add(v1.get(i));
                list.add(v2.get(i));
            }
            for (int i = v2.size(); i < v1.size(); i++)
                list.add(v1.get(i));
        }
        else
        {
            for (int i = 0; i < v1.size(); i++)
            {
                list.add(v1.get(i));
                list.add(v2.get(i));
            }
            for (int i = v1.size(); i < v2.size(); i++)
                list.add(v2.get(i));
        }
    }

    public int next() {
        return list.get(index++);
    }

    public boolean hasNext() {
        return index < list.size();
    }
}

/**
 * Your ZigzagIterator object will be instantiated and called as such:
 * ZigzagIterator i = new ZigzagIterator(v1, v2);
 * while (i.hasNext()) v[f()] = i.next();
 */