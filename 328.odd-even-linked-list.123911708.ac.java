/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode oddEvenList(ListNode head) {
        if (head == null)
            return null;
        ListNode current = head;
        ListNode oddDummy = new ListNode(0);
        ListNode evenDummy = new ListNode(0);
        ListNode odd = oddDummy;
        ListNode even = evenDummy;
        int count = 1;
        while (current != null)
        {
            if (count % 2 == 0)
            {
                even.next = current;
                even = even.next;
                current = current.next;
                even.next = null;
            }
            else
            {
                odd.next = current;
                odd = odd.next;
                current = current.next;
                odd.next = null;
            }
            count++;
        }
        odd.next = evenDummy.next;
        return oddDummy.next;
    }
}