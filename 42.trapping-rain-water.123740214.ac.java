class Solution {
    public int trap(int[] height) {
        ArrayList<Integer> list = new ArrayList<>();
        int answer = 0;
        if (height.length <= 1)
            return 0;
        for (int i = 0; i < height.length; i++)
        {
            if (list.size() == 0)
                list.add(height[i]);
            else
            {
                if (list.get(0) > height[i])
                    list.add(height[i]);
                else
                {
                    for (int j = 1; j < list.size(); j++)
                        answer += list.get(0) - list.get(j);
                    list.clear();
                    list.add(height[i]);
                }
            }
        }
        int[] remain = new int[list.size()];
        for (int i = 0; i < list.size(); i++)
            remain[i] = list.get(list.size() - 1 - i);
        return answer + trap(remain);
    }
}