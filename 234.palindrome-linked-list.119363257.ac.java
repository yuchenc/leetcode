/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean isPalindrome(ListNode head) {
        int length = 0;
        ListNode first = head;
        while (first != null)
        {
            length++;
            first = first.next;
        }
        int middle = length / 2;
        first = head;
        Stack<Integer> q = new Stack<>();
        for (int i = 0; i < middle; i++)
        {
            q.push(first.val);
            first = first.next;
        }
        if (length % 2 == 1)
            first = first.next;
        for (int i = 0; i < middle; i++)
        {
            if (q.peek() != first.val)
                return false;
            q.pop();
            first = first.next;
        }
        return true;
    }
}