/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode sortList(ListNode head) {
        if (head == null || head.next == null)
            return head;
        ListNode slow = head;
        ListNode prev = null;
        ListNode fast = head;
        while (fast != null && fast.next != null)
        {
            prev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        prev.next = null;
        ListNode left = sortList(head);
        ListNode right = sortList(slow);
        ListNode answer = merge(left, right);
        return answer;
    }
    
    public ListNode merge(ListNode left, ListNode right) {
        ListNode head = new ListNode(0);
        ListNode current = head;
        while (left != null && right != null)
        {
            if (left.val < right.val)
            {
                current.next = left;
                left = left.next;
                current = current.next;
            }
            else
            {
                current.next = right;
                right = right.next;
                current = current.next;
            }
        }
        if (left == null)
            current.next = right;
        else
            current.next = left;
        return head.next;
    }
}