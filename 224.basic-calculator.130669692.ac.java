class Solution {
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<>();
        int res = 0;
        int sig = 1;
        int number = 0;
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if (c >= '0' && c <= '9')
                number = number * 10 + (int)(c - '0');
            else if (c == '+')
            {
                res += number * sig;
                number = 0;
                sig = 1;
            }
            else if (c == '-')
            {
                res += number * sig;
                number = 0;
                sig = -1;
            }
            else if (c == '(')
            {
                stack.push(res);
                stack.push(sig);
                sig = 1;
                res = 0;
            }
            else if (c == ')')
            {
                res += number * sig;
                number = 0;
                res *= stack.pop();
                res += stack.pop();
            }
        }
        if (number != 0)
            res += number * sig;
        return res;
    }
}