class Solution {
    public int maxProfit(int[] prices) {
        if (prices.length == 0)
            return 0;
        int answer = 0;
        int min = prices[0];
        for (int i = 1; i < prices.length; i++)
        {
            if (prices[i] < min)
                min = prices[i];
            else    
                answer = Math.max(prices[i] - min, answer);
        }
        return answer;
    }
}
