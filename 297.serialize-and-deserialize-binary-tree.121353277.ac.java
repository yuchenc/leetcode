/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (root == null)
            return "N";
        String answer = "" + root.val + ",";
        answer += serialize(root.left) + ",";
        answer += serialize(root.right);
        //System.out.println(answer);
        return answer;
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Queue<String> queue = new LinkedList<>();
        String[] vals = data.split(",");
        for (int i = 0; i < vals.length; i++)
            queue.offer(vals[i]);
        return deserialize(queue);
    }
    
    public TreeNode deserialize(Queue<String> queue)
    {
        String s = queue.poll();
        if (s.equals("N"))
            return null;
        TreeNode root = new TreeNode(Integer.parseInt(s));
        root.left = deserialize(queue);
        root.right = deserialize(queue);
        return root;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));