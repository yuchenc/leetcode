/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public boolean hasCycle(ListNode head) {
        if (head == null)
            return false;
        if (head.next == null)
            return false;
        ListNode first = head;
        ListNode second = head.next;
        while (second != null && second.next != null && first != second)
        {
            first = first.next;
            second = second.next.next;
        }
        if (second != null && second.next != null)
            return true;
        else
            return false;
    }
}