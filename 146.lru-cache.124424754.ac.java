class LRUCache {
    class CacheNode {
        int value;
        int key;
        CacheNode next;
        CacheNode prev;
        CacheNode(int key, int val) {
            this.value = val;
            this.key = key;
        }
    }
    private HashMap<Integer, CacheNode> map;
    private int size;
    private CacheNode head;
    private CacheNode tail;
    
    public LRUCache(int capacity) {
        map = new HashMap<>();
        size = capacity;
        head = new CacheNode(-1,0);
        tail = new CacheNode(-1,0);
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        if (map.containsKey(key))
        {
            CacheNode current = map.get(key);
            moveToTail(current);
            return current.value;
        }
        else
            return -1;
    }
    
    public void put(int key, int value) {
        if (map.containsKey(key))
        {
            CacheNode current = map.get(key);
            current.value = value;
            moveToTail(current);
        }
        else
        {
            if (map.size() == size)
            {
                CacheNode newnode = new CacheNode(key, value);
                map.put(key, newnode);
                map.remove(head.next.key);
                head.next.next.prev = head;
                head.next = head.next.next;
                newnode.prev = tail.prev;
                tail.prev.next = newnode;
                newnode.next = tail;
                tail.prev = newnode;
            }
            else
            {
                CacheNode newnode = new CacheNode(key, value);
                map.put(key, newnode);
                newnode.prev = tail.prev;
                tail.prev.next = newnode;
                newnode.next = tail;
                tail.prev = newnode;
            }
        }    
    }
    
    public void moveToTail(CacheNode current) {
        current.prev.next = current.next;
        current.next.prev = current.prev;
        current.prev = tail.prev;
        tail.prev.next = current;
        current.next = tail;
        tail.prev = current;
    }
    
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */