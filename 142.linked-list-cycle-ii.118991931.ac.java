/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode detectCycle(ListNode head) {
        if (head == null)
            return null;
        ListNode first = head.next;
        if (first == null)
            return null;
        ListNode second = head.next.next;
        while (second != null && second.next != null && first != second)
        {
            first = first.next;
            second = second.next.next;
        }
        if (second == null || second.next == null)
            return null;
        first = head;
        while (first != second)
        {
            first = first.next;
            second = second.next;
        }
        return first;
    }
}