class Solution {
    class Trie {
        String val;
        boolean isWord;
        Trie[] child;
        public Trie (String s) {
            val = s;
            child = new Trie[26];
        }
    }
    
    public List<String> findWords(char[][] board, String[] words) {
        List<String> answer = new ArrayList<>();
        if (board.length == 0 || board[0].length == 0)
            return answer;
        Trie root = new Trie("");
        for (String str : words)
        {
            Trie current = root;
            for (int i = 0; i < str.length(); i++)
            {
                char c = str.charAt(i);
                if (current.child[c-'a'] == null)
                    current.child[c-'a'] = new Trie(str.substring(0,i+1));
                current = current.child[c-'a'];
                if (i == str.length() - 1)
                    current.isWord = true;
            }
        }
        for (int i = 0; i < board.length; i++)
        {
            for (int j = 0; j < board[0].length; j++)
            {
                boolean[][] visited = new boolean[board.length][board[0].length];
                dfs(board, root, i, j, answer, visited);
            }
        }
        return answer;
    }
    
    public void dfs(char[][] board, Trie node, int x, int y, List<String> answer, boolean[][] visited) {
        if (node.isWord)
        {
            node.isWord = false;
            answer.add(node.val);
            return;
        }
        if (x < 0 || x >= board.length || y < 0 || y >= board[0].length)
            return;
        if (visited[x][y])
            return;
        char c = board[x][y];
        if (node.child[c-'a']==null)
            return;
        else
        {
            visited[x][y] = true;
            dfs(board, node.child[c-'a'], x+1, y, answer, visited);
            dfs(board, node.child[c-'a'], x, y+1, answer, visited);
            dfs(board, node.child[c-'a'], x-1, y, answer, visited);
            dfs(board, node.child[c-'a'], x, y-1, answer, visited);
            visited[x][y] = false;
        }
    }
}