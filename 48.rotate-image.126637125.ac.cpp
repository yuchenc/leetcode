class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        if (matrix.size() == 0 || matrix[0].size() == 0)
            return;
        transpose(matrix);
        swap(matrix);
    }
    
    void transpose(vector<vector<int>>& matrix) {
        for (int i = 0; i < matrix.size(); i++)
        {
            for (int j = i+1; j < matrix[0].size(); j++)
            {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tmp;
            }
        }
    }
    
    void swap(vector<vector<int>>& matrix) {
        for (int i = 0; i < matrix.size(); i++)
        {
            for (int j = 0; j < matrix[0].size() / 2; j++)
            {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[i][matrix[0].size()- 1 - j];
                matrix[i][matrix[0].size()- 1 - j] = tmp;
            }
        }
    }
};