class Solution {
    public boolean canPermutePalindrome(String s) {
        HashSet<Character> counts = new HashSet<>();
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if (counts.contains(c))
                counts.remove(c);
            else
                counts.add(c);
        }
        return counts.size() <= 1;
        
    }
}