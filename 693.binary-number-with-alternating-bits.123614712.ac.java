class Solution {
    public boolean hasAlternatingBits(int n) {
        int prev = n & 1;
        while(n != 0)
        {
            n = n >> 1;
            int current = n & 1;
            if (prev == current)
                return false;
            prev = current;
        }
        return true; 
    }
}