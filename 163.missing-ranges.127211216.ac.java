class Solution {
    public List<String> findMissingRanges(int[] nums, int lower, int upper) {
        List<String> res = new ArrayList<>();
        if (upper < lower)
            return res;
        if (nums.length == 0)
        {
            if (lower == upper)
                res.add(String.valueOf(lower));
            else
                res.add(String.valueOf(lower) + "->" + String.valueOf(upper));
            return res;
        }
        if (nums[0] != Integer.MIN_VALUE && lower == nums[0] - 1)
            res.add(String.valueOf(lower));
        else if (nums[0] != Integer.MIN_VALUE && lower < nums[0] - 1)
            res.add(String.valueOf(lower) + "->" + String.valueOf(nums[0]-1));
        for (int i = 1; i < nums.length; i++)
        {
            int left = nums[i-1];
            int right = nums[i];
            if (left == right || left == right - 1)
                continue;
            if (left == right - 2)
                res.add(String.valueOf(right-1));
            else if (left < right - 2)
                res.add(String.valueOf(left+1) + "->" + String.valueOf(right-1));
        }
        if (nums[nums.length-1] != Integer.MAX_VALUE && upper == nums[nums.length-1] + 1)
            res.add(String.valueOf(upper));
        else if (nums[nums.length-1] != Integer.MAX_VALUE && upper > nums[nums.length-1] + 1)
            res.add(String.valueOf(nums[nums.length-1]+1) + "->" + String.valueOf(upper));
        return res;
        
    }
}