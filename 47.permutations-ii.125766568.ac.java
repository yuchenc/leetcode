class Solution {
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> answer = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        if (nums.length == 0)
        {
            answer.add(list);
            return answer;
        }
        Arrays.sort(nums);
        boolean[] visited = new boolean[nums.length];
        recursion(nums, visited, list, answer);
        return answer;
    }
    
    public void recursion(int[] nums, boolean[] visited, List<Integer> list, List<List<Integer>> answer) {
        if (list.size() == nums.length)
            answer.add(new ArrayList<>(list));
        for (int i = 0; i < nums.length ; i++)
        {
            if (visited[i])
                continue;
            if (i > 0 && nums[i] == nums[i-1] && !visited[i-1])
                continue;
            visited[i] = true;
            list.add(nums[i]);
            recursion(nums, visited, list, answer);
            visited[i] = false;
            list.remove(list.size() - 1);
        }
    }
}