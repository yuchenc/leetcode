class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> answer = new ArrayList<Integer>();
        if (s.length() < 1 || p.length() < 1 || p.length() > s.length())
            return answer;
        int left = 0;
        int right = 0;
        int sLen = s.length();
        int pLen = p.length();
        int[] hash = new int[128];
        for (int i = 0; i < pLen; i++)
            hash[p.charAt(i)]++;
        int count = pLen;
        while (right < sLen)
        {
            if (hash[s.charAt(right++)]-- >= 1)
                count--;
            if (count == 0)
                answer.add(left);
            if (right - left == pLen && hash[s.charAt(left++)]++ >= 0)
                count++;
        }
        return answer;
    }
}