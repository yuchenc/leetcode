class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        int[][] matrix = new int[numCourses][numCourses];
        int[] preOfCourse = new int[numCourses];
        for (int i = 0; i < prerequisites.length; i++)
        {
            int course = prerequisites[i][0];
            int pre = prerequisites[i][1];
            if (matrix[pre][course] == 0)
            {
                matrix[pre][course] = 1;
                preOfCourse[course]++;
            }
        }
        int count = 0;
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < numCourses; i++)
        {
            if (preOfCourse[i] == 0)
                queue.offer(i);
        }
        while (!queue.isEmpty())
        {
            int course = queue.poll();
            count++;
            for (int i = 0; i < numCourses; i++)
            {
                if (matrix[course][i] == 1)
                {
                    if (--preOfCourse[i] == 0)
                        queue.offer(i);
                }
            }   
        }
        return count == numCourses;
    }
}