/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> answer = new ArrayList<>();
        getAnswer(root, answer, 1);
        return answer;
    }
    public void getAnswer(TreeNode node, List<List<Integer>> answer, int depth)
    {
        if (node == null)
            return;
        if (depth > answer.size())
        {
            List<Integer> tmp = new ArrayList<>();
            tmp.add(node.val);
            answer.add(tmp);
        }
        else
            answer.get(depth - 1).add(node.val);
        getAnswer(node.left, answer, depth + 1);
        getAnswer(node.right, answer, depth + 1);
    }
}