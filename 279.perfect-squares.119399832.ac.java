class Solution {
    public int numSquares(int n) {
        int answer[] = new int[n+1];
        for (int i = 1; i <= n; i++)
        {
            int a = (int) Math.sqrt(i);
            if (i == a*a)
                answer[i] = 1;
            else
            {
                answer[i] = 1 + answer[i - a*a];
                for (int j = a-1; j > 0; j--)
                    answer[i] = Math.min(answer[i], 1 + answer[i - j*j]);
            }
        }
        return answer[n];
    }
}