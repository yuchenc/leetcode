class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        std::map<int, int> map;
        vector<int> answer;
        for (int i = 0; i < nums.size(); i++)
            map.insert(make_pair(nums[i], i));
        for (int i = 0; i < nums.size(); i++)
        {
            int complement = target - nums[i];
            if (map.find(complement) != map.end() && map.find(complement)->second != i)
            {
                answer.push_back(i);
                answer.push_back(map.find(complement)->second);
                return answer;
                    
            }   
        }
        return answer;
    }
};