class Solution {
    public int maxProfit(int[] prices) {
        int answer = 0;
        int current = 0;
        for (int i = 1; i < prices.length; i++)
        {
            current = Math.max(0, current + prices[i] - prices[i-1]);
            answer = Math.max(answer, current);
        }
        return answer;
    }
}