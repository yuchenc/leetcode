/**
 * Definition for undirected graph.
 * class UndirectedGraphNode {
 *     int label;
 *     List<UndirectedGraphNode> neighbors;
 *     UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
 * };
 */
public class Solution {
    HashMap<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if (node == null)
            return null;
        UndirectedGraphNode clone = new UndirectedGraphNode(node.label);
        map.put(node, clone);
        for (int i = 0; i < node.neighbors.size(); i++)
        {
            UndirectedGraphNode tmp = node.neighbors.get(i);
            if (map.containsKey(tmp))
                clone.neighbors.add(map.get(tmp));
            else
                clone.neighbors.add(cloneGraph(node.neighbors.get(i)));
        }
        return clone;
    }
}