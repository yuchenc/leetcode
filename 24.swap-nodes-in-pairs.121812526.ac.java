/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode swapPairs(ListNode head) {
        if (head == null)
            return null;
        if (head.next == null)
            return head;
        ListNode current = head;
        head = current.next;
        current.next = head.next;
        head.next = current;
        while (current.next != null && current.next.next != null)
        {
            ListNode next1 = current.next;
            ListNode next2 = current.next.next;
            current.next = next2;
            next1.next = next2.next;
            next2.next = next1;
            current = next1;
        }
        return head;
        
        
    }
}