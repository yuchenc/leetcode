class Solution {
    public int findLUSlength(String a, String b) {
        if (b.length() > a.length())
            return b.length();
        else if (b.length() < a.length())
            return a.length();
        else
        {
            if (a.equals(b))
                return -1;
            else
                return a.length();
        }
    }
}