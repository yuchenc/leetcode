/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<Double> averageOfLevels(TreeNode root) {
        List<List<Integer>> list = new ArrayList<>();
        goThrough(root, list, 1);
        List<Double> res = new ArrayList<>();
        for (int i = 0; i < list.size(); i++)
        {
            double tmp = 0;
            for (int j = 0; j < list.get(i).size(); j++)
                tmp += list.get(i).get(j);
            res.add(tmp / list.get(i).size());
        }
        return res;
    }
    
    public void goThrough(TreeNode root, List<List<Integer>> list, int level) {
        if (root == null)
            return;
        if (list.size() < level)
            list.add(new ArrayList<Integer>());
        list.get(level-1).add(root.val);
        goThrough(root.left, list, level + 1);
        goThrough(root.right, list, level + 1);
    }
}