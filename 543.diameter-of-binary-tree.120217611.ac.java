/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    int answer = 0;
    public int diameterOfBinaryTree(TreeNode root) {
        depth(root);
        return answer;
    }
    public int depth(TreeNode root)
    {
        if (root == null)
            return 0;
        int left = depth(root.left);
        int right = depth(root.right);
        answer = Math.max(answer, left + right);
        return Math.max(left, right) + 1;
    }
}