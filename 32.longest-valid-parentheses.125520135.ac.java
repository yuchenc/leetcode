class Solution {
    public int longestValidParentheses(String s) {
        int max = 0;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == '(')
                stack.push(i);
            else
            {
                if (stack.isEmpty())
                    stack.push(i);
                else
                {
                    int b = stack.peek();
                    if (s.charAt(b) == '(')
                        stack.pop();
                    else
                        stack.push(i);
                }
            }
        }
        int right = s.length();
        if (stack.isEmpty())
            return right;
        else
        {
            while (!stack.isEmpty())
            {
                int b = stack.pop();
                max = Math.max(max, right - b-1);
                right = b;
            }
            max = Math.max(max, right);
        }
        return max;
    }
}