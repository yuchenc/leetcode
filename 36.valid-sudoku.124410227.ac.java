class Solution {
    public boolean isValidSudoku(char[][] board) {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (board[i][j] != '.')
                    if (!check(board, i, j))
                        return false;
            }
        }
        return true;
    }
    
    public boolean check(char[][] board, int x, int y) {
        for (int i = 0; i < 9; i++)
        {
            if (i != x && board[i][y] == board[x][y])
                return false;
            if (i != y && board[x][i] == board[x][y])
                return false;
        }
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (i + x/3*3 == x && j + y/3*3 == y)
                    continue;
                else if (board[i+x/3*3][j+y/3*3] == board[x][y])
                    return false;
            }
        }
        return true;
    }
}