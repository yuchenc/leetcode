import java.util.Hashtable; 

class LRUCache {
    class DLinkedNode
    {
        int key;
        int value;
        DLinkedNode pre;
        DLinkedNode next;
    }
    
    private void addNode (DLinkedNode node)
    {
        node.pre = head;
        node.next = head.next;
        head.next.pre = node;
        head.next = node;
    }
    
    private void removeNode (DLinkedNode node)
    {
        node.pre.next = node.next;
        node.next.pre = node.pre;
    }
    
    private void moveToHead(DLinkedNode node)
    {
	    this.removeNode(node);
	    this.addNode(node);
    }
    
    private DLinkedNode popTail(){
	    DLinkedNode res = tail.pre;
	    this.removeNode(res);
	    return res;
    }
    
    private Hashtable<Integer, DLinkedNode> cache;
    private int count;
    private int capacity;
    private DLinkedNode head, tail;

    public LRUCache(int capacity) {
        cache = new Hashtable<Integer, DLinkedNode>();
        this.count = 0;
        this.capacity = capacity;
        this.head = new DLinkedNode();
        this.tail = new DLinkedNode();
        head.pre = null;
        tail.next = null;
        head.next = tail;
        tail.pre = head;
    }
    
    public int get(int key) {
        if (cache.containsKey(key))
        {
            DLinkedNode node = cache.get(key);
            this.moveToHead(node);
            return node.value;
        }
        else
            return -1;
    }
    
    public void put(int key, int value) {
        if (cache.containsKey(key))
        {
            DLinkedNode node = cache.get(key);
            this.moveToHead(node);
            node.value = value;
        }
        else
        {
            if (count == capacity)
            {
                int deleteKey = tail.pre.key;
                cache.remove(deleteKey);
                this.removeNode(tail.pre);
                DLinkedNode node = new DLinkedNode();
                node.key = key;
                node.value = value;
                this.addNode(node);
                cache.put(key, node);
            }
            else
            {
                DLinkedNode node = new DLinkedNode();
                node.key = key;
                node.value = value;
                this.addNode(node);
                count++;
                cache.put(key, node);
            }
        }
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */