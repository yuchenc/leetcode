class Solution {
    public int removeDuplicates(int[] nums) {
        int answer = 0;
        if (nums.length == 0)
            return answer;
        answer++;
        for(int i = 1; i < nums.length; i++)
        {
            if (nums[i] == nums[i-1])
                continue;
            else
            {
                nums[answer] = nums[i];
                answer++;
            }
        }
        return answer;
    }
}