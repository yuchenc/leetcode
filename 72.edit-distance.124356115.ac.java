class Solution {
    public int minDistance(String word1, String word2) {
        int[][] result = new int[2][word2.length()+1];
        for (int i = 0; i <= word2.length(); i++)
            result[0][i] = i;
        for (int i = 1; i <= word1.length(); i++)
        {
            result[i%2][0] = i;
            for (int j = 1; j <= word2.length(); j++)
            {
                result[i%2][j] = Math.min(result[(i-1)%2][j]+1, Math.min(result[i%2][j-1]+1, result[(i-1)%2][j-1] + (word1.charAt(i-1) == word2.charAt(j-1) ? 0 : 1)));
            }
        }
        return result[word1.length()%2][word2.length()];
    }
}