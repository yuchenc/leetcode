class Solution {
public:
    int maxArea(vector<int>& height) {
        int left = 0;
        int right = height.size() - 1;
        int max = 0;
        int area;
        while (left != right)
        {
            if (height.at(left) < height.at(right))
            {
                area = (right - left) * height.at(left);
                if (area > max)
                    max = area;
                left++;
            }
            else
            {
                area = (right - left) * height.at(right);
                if (area > max)
                    max = area;
                right--;
            }
        }
        return max;
    }
};