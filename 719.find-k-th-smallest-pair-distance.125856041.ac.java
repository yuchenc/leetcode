class Solution {
    public int smallestDistancePair(int[] nums, int k) {
        int[] count = new int[1000000];
        for (int diff = 1; diff < nums.length; diff++)
        {
            for (int j = 0; j < nums.length - diff; j++)
            {
                count[Math.abs(nums[j]-nums[j+diff])]++;
            }
        }
        int i = 0;
        for (; i < 1000000; i++)
        {
            if (count[i] >= k)
                break;
            else
            {
                k = k - count[i];
            }
        }
        return i;
    }
}