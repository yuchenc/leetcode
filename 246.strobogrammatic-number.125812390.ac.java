class Solution {
    public boolean isStrobogrammatic(String num) {
        for (int i = 0; i < num.length() / 2; i++)
        {
            if (num.charAt(i) == '8' && num.charAt(num.length() - 1 - i) == '8')
                continue;
            else if (num.charAt(i) == '1' && num.charAt(num.length() - 1 - i) == '1')
                continue;
            else if (num.charAt(i) == '0' && num.charAt(num.length() - 1 - i) == '0')
                continue;
            else if (num.charAt(i) == '6' && num.charAt(num.length() - 1 - i) == '9')
                continue;
            else if (num.charAt(i) == '9' && num.charAt(num.length() - 1 - i) == '6')
                continue;
            else
                return false;
        }
        if (num.length() % 2 == 1)
            if (num.charAt(num.length() / 2) != '1' && num.charAt(num.length() / 2) != '8' && num.charAt(num.length() / 2) != '0')
                return false;
        return true;
    }
}