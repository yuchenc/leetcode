class Solution {
public:
    string longestPalindrome(string s) {
        if (s == "")
            return s;
        string answer;
        answer.push_back(s[0]);
        string current;
        for (int i = 1; i < s.length(); i++)
        {
            current.clear();
            current = s[i];
            for (int j = 1; j <= i && j < s.length() - i; j++)
            {
                if (s[i-j] == s[i+j])
                {
                    current = s.substr(i-j, 2*j + 1);
                }
                else
                {
                    break;
                }
            }
            if (current.length() > answer.length())
                answer = current;
        }
        for (int i = 1; i < s.length(); i++)
        {
            current.clear();
            if (s[i-1] == s[i])
            {
                current = s.substr(i-1, 2);
                for (int j = 1; j <= i-1 && j < s.length() - i; j++)
                {
                    if (s[i-j-1] == s[i+j])
                    {
                        current = s.substr(i-j-1, 2*j + 2);
                    }
                    else
                    {
                        break;
                    }
                }
                if (current.length() > answer.length())
                    answer = current;
            }
        }
        return answer;
    }
};