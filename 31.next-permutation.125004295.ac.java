class Solution {
    public void nextPermutation(int[] nums) {
        if (nums.length < 1)
            return;
        int i = nums.length - 2;
        for (; i >= 0; i--)
            if (nums[i] < nums[i+1])
                break;
        if (i < 0)
        {
            reserve(nums, 0, nums.length-1);
            return;
        }
        else
        {
            int val = nums[i];
            int j = nums.length - 1;
            for (; j > i; j--)
            {
                if (nums[j] > val)
                    break;
            }
            swap(nums, i, j);
            reserve(nums, i+1, nums.length -1);
        }
    }
    public void swap (int[] nums, int x, int y) {
        int tmp = nums[x];
        nums[x] = nums[y];
        nums[y] = tmp;
    }
    public void reserve(int[] nums, int x, int y)
    {
        while (x < y)
        {
            int tmp = nums[x];
            nums[x] = nums[y];
            nums[y] = tmp;
            x++;
            y--;
        }
    }
}