class Solution {
    int diff = Integer.MAX_VALUE;
    String res = "";
    
    public String nextClosestTime(String time) {
        HashSet<Integer> set = new HashSet<>();
        set.add(Integer.parseInt(time.substring(0,1)));
        set.add(Integer.parseInt(time.substring(1,2)));
        set.add(Integer.parseInt(time.substring(3,4)));
        set.add(Integer.parseInt(time.substring(4,5)));
        if (set.size() == 1)
            return time;
        List<Integer> list = new ArrayList<>(set);
        int minute = Integer.parseInt(time.substring(0,2)) * 60 + Integer.parseInt(time.substring(3));
        dfs(list, "", 0, minute);
        return res;
    }
    
    public void dfs(List<Integer> list, String current, int pos, int minute) {
        if (pos == 4)
        {
            int m = Integer.parseInt(current.substring(0,2)) * 60 + Integer.parseInt(current.substring(2));
            if (m == minute)
                return;
            int d = (m > minute ? m - minute : m + 1440 + m - minute);
            if (d < diff)
            {
                diff = d;
                res = current.substring(0,2) + ":" + current.substring(2);
            }
            return;
        }
        for (int i = 0; i < list.size(); i++)
        {
            if (pos == 0 && list.get(i) > 2)
                continue;
            if (pos == 1 && Integer.parseInt(current)*10 + list.get(i) > 23)
                continue;
            if (pos == 2 && list.get(i) > 5)
                continue;
            if (pos == 3 && Integer.parseInt(current.substring(2))*10 + list.get(i) > 59)
                continue;
            dfs(list, current + list.get(i), pos + 1, minute);
        }
    }
}