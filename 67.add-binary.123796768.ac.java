class Solution {
    public String addBinary(String a, String b) {
        ArrayList<Character> list = new ArrayList<>();
        int len1 = a.length();
        int len2 = b.length();
        if (len2 > len1)
            return addBinary(b, a);
        int index = 0;
        int flag = 0;
        while (index < len2)
        {
            int l = a.charAt(len1 - index - 1) - '0';
            int r = b.charAt(len2 - index - 1) - '0';
            char c = (char)((l + r + flag)%2 + '0');
            list.add(c);
            if (l + r + flag > 1)
                flag = 1;
            else
                flag = 0;
            index++;
        }
        while (index < len1)
        {
            int l = a.charAt(len1 - index - 1) - '0';
            char c = (char)((l + flag)%2 + '0');
            list.add(c);
            if (l + flag > 1)
                flag = 1;
            else
                flag = 0;
            index++;
        }
        if (flag == 1)
            list.add('1');
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++)
            sb.append(list.get(list.size() - 1 - i));
        return sb.toString();
    }
}