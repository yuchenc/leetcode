class Solution {
    public int rob(int[] nums) {
        int prevNo = 0;
        int prevYes = 0;
        for (int i = 0; i < nums.length; i++)
        {
            int tmp = prevNo;
            prevNo = Math.max(prevYes, prevNo);
            prevYes = tmp + nums[i];
        }
        return Math.max(prevNo, prevYes);
    }
}