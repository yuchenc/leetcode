class Solution {
    public int rob(int[] nums) {
        int answer = 0;
        if (nums.length == 0)
            return 0;
        if (nums.length == 1)
            return nums[0];
        if (nums.length == 2)
            return Math.max(nums[0], nums[1]);
        if (nums.length == 3)
            return Math.max(nums[0], Math.max(nums[1], nums[2]));
        int rob = nums[2];
        int no = 0;
        answer = Math.max(answer, nums[0] + rob);
        for (int i = 3; i < nums.length - 1; i++)
        {
            int tmp = rob;
            rob = no + nums[i];
            no = Math.max(tmp, no);
            answer = Math.max(answer, rob + nums[0]);
            answer = Math.max(answer, no + nums[0]);
        }
        rob = nums[1];
        no = 0;
        for (int i = 2; i < nums.length; i++)
        {
            int tmp = rob;
            rob = no + nums[i];
            no = Math.max(tmp, no);
            answer = Math.max(answer, rob);
            answer = Math.max(answer, no);
        }
        return answer;
    }
}