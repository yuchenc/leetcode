class Solution {
    public int maxProduct(int[] nums) {
        if (nums.length == 0)
            return 0;
        int max = nums[0];
        int min = nums[0];
        int answer = max;
        for (int i = 1; i < nums.length; i++)
        {
            if (nums[i] < 0)
            {
                if (min < 0 && max > 0)
                {
                    int tmp = max;
                    max = min * nums[i];
                    min = tmp * nums[i];
                }
                else if (min < 0 && max < 0)
                {
                    max = min * nums[i];
                    min = nums[i];
                }
                else if (min > 0 && max > 0)
                {
                    min = max * nums[i];
                    max = nums[i];
                }
                else if (min == 0)
                {
                    max = nums[i];
                    min = nums[i];
                }
                if (max > answer)
                    answer = max;
            }
            else if (nums[i] == 0)
            {
                max = 0;
                min = 0;
                if (max > answer)
                    answer = max;
            }
            else
            {
                if (max > 0 && min < 0)
                {
                    max = max * nums[i];
                    min = min * nums[i];
                }
                else if (max < 0 && min < 0)
                {
                    max = nums[i];
                    min = min * nums[i];
                }
                else if (max > 0 && min > 0)
                {
                    max = max * nums[i];
                    min = nums[i];
                }
                else if (min == 0)
                {
                    max = nums[i];
                    min = nums[i];
                }
                if (max > answer)
                    answer = max;
            }
        }
        return answer;
    }
}