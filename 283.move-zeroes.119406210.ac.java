class Solution {
    public void moveZeroes(int[] nums) {
        int left = 0;
        int right = nums.length - 1;
        int[] tmp = new int[right + 1];
        for (int i = 0; i < nums.length; i++)
        {
            if (nums[i] == 0)
            {
                tmp[right] = 0;
                right--;
            }
            else
            {
                tmp[left] = nums[i];
                left++;
            }
        }
        for (int i = 0; i < nums.length; i++)
            nums[i] = tmp[i];
    }
}