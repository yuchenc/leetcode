class Solution {
    public int largestPalindrome(int n) {
        if (n == 1)
            return 9;
        int upperBound = (int) Math.pow(10, n)-1;
        int lowerBound = upperBound / 10;
        long max = (long) upperBound * (long) upperBound;
        int firstHalf = (int)(max / (long) Math.pow(10, n));
        
        boolean palindromFound = false;
        long palindrom = 0;
        
        while (!palindromFound) {
            palindrom = createPalindrom(firstHalf);
            for (long i = upperBound; upperBound > lowerBound; i--) { 
                if (palindrom / i > max || i * i < palindrom) {
                    break;
                }
                
                if (palindrom % i == 0) {
                    palindromFound = true;
                    break;
                }
            }

            firstHalf--;
        }
        return (int) (palindrom % 1337);
    }
    
    private long createPalindrom(long firstHalf) {
        String str = firstHalf + new StringBuilder().append(firstHalf).reverse().toString();
        return Long.parseLong(str);
    }
}