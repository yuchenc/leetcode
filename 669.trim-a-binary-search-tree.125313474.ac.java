/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public TreeNode trimBST(TreeNode root, int L, int R) {
        if (root == null)
            return null;
        if (root.val >= L && root.val <= R)
        {
            TreeNode res = new TreeNode(root.val);
            res.left = trimBST(root.left, L, R);
            res.right = trimBST(root.right, L, R);
            return res;
        }
        else if (root.val < L)
            return trimBST(root.right, L, R);
        else
            return trimBST(root.left, L, R);
        
    }
}