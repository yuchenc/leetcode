class Solution {
    public List<String> letterCasePermutation(String S) {
        List<String> res = new ArrayList<>();
        res.add(S);
        for (int i = 0; i < S.length(); i++) {
            int l = res.size();
            if (S.charAt(i) >= 'a' && S.charAt(i) <= 'z') {
                for (int j = 0; j < l; j++) {
                    String change = res.get(j).substring(0, i) + Character.toUpperCase(S.charAt(i)) + res.get(j).substring(i+1);
                    res.add(change);
                }
            }
            else if (S.charAt(i) >= 'A' && S.charAt(i) <= 'Z') {
                for (int j = 0; j < l; j++) {
                    String change = res.get(j).substring(0, i) + Character.toLowerCase(S.charAt(i)) + res.get(j).substring(i+1);
                    res.add(change);
                }
            }
        }
        return res;
    }
}