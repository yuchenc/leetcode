class Solution {
    public List<String> generateParenthesis(int n) {
        List<List<String>> answerList = new ArrayList<>();
        String f0 = "";
        List<String> f0list = new ArrayList<>();
        f0list.add(f0);
        answerList.add(f0list);
        for (int i = 1; i <= n; i++)
        {
            List<String> tmpanswer = new ArrayList<>();
            for (int j = 0; j < i; j++)
            {
                for (String first : answerList.get(j))
                {
                    for (String second : answerList.get(i - 1 - j))
                    {
                        tmpanswer.add("(" + first + ")" + second);
                    }
                }
            }
            answerList.add(tmpanswer);
        }
        return answerList.get(answerList.size() - 1);
        
        
        
    }
}