class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int answer;
        int sum;
        if (nums.size() < 3)
            return 0;
        unsigned int threshold = -1; // magic number
        for (int i = 0; i < nums.size() - 2; i++)
        {
            int left = i + 1;
            int right = nums.size() - 1;
            sum = nums.at(i) + nums.at(left) + nums.at(right);
            while (left != right)
            {
                sum = nums.at(i) + nums.at(left) + nums.at(right);
                if (sum - target < 0)
                {
                    left++;
                    if (target - sum < threshold)
                    {
                        threshold = target - sum;
                        answer = sum;
                    }
                }
                else
                {
                    right--;
                    if (sum - target < threshold)
                    {
                        answer = sum;
                        threshold = sum - target;
                    }
                }
            }
        }
        return answer;
    }
};