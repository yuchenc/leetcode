class Solution {
public:
    vector<string> letterCombinations(string digits) {
        vector<string> answer;
        answer.clear();
        vector<string> tmpVector;
        if (digits.size() < 1)
            return answer;
        if (digits[0] >= 50 && digits[0] < 55)
        {
            string tmpString;
            for (int i = 0; i < 3; i++)
            {
                tmpString.clear();
                tmpString.push_back(digits[0] * 3 - 53 + i);
                answer.push_back(tmpString);
            }
        }
        else if (digits[0] == 55)
        {
            string tmpString;
            for (int i = 0; i < 4; i++)
            {
                tmpString.clear();
                tmpString.push_back(digits[0] * 3 - 53 + i);
                answer.push_back(tmpString);
            }
        }
        else if (digits[0] == 56)
        {
            string tmpString;
            for (int i = 0; i < 3; i++)
            {
                tmpString.clear();
                tmpString.push_back(digits[0] * 3 - 52 + i);
                answer.push_back(tmpString);
            }
        }
        else if (digits[0] == 57)
        {
            string tmpString;
            for (int i = 0; i < 4; i++)
            {
                tmpString.clear();
                tmpString.push_back(digits[0] * 3 - 52 + i);
                answer.push_back(tmpString);
            }
        }
        for (int i = 1; i < digits.length(); i++)
        {
            string tmpString;
            if (digits[i] >= 50 && digits[i] < 55)
            {
                tmpVector.clear();
                for (int j = 0; j < answer.size(); j++)
                {
                    for (int k = 0; k < 3; k++)
                    {
                        tmpString.clear();
                        tmpString = answer.at(j);
                        tmpString.push_back(digits[i] * 3 - 53 + k);
                        tmpVector.push_back(tmpString);
                    }
                }
                answer.swap(tmpVector);
            }
            else if (digits[i] == 55)
            {
                tmpVector.clear();
                for (int j = 0; j < answer.size(); j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        tmpString.clear();
                        tmpString = answer.at(j);
                        tmpString.push_back(digits[i] * 3 - 53 + k);
                        tmpVector.push_back(tmpString);
                    }
                }
                answer.swap(tmpVector);
            }
            else if (digits[i] == 56)
            {
                tmpVector.clear();
                for (int j = 0; j < answer.size(); j++)
                {
                    for (int k = 0; k < 3; k++)
                    {
                        tmpString.clear();
                        tmpString = answer.at(j);
                        tmpString.push_back(digits[i] * 3 - 52 + k);
                        tmpVector.push_back(tmpString);
                    }
                }
                answer.swap(tmpVector);
            }
            else if (digits[i] == 57)
            {
                tmpVector.clear();
                for (int j = 0; j < answer.size(); j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        tmpString.clear();
                        tmpString = answer.at(j);
                        tmpString.push_back(digits[i] * 3 - 52 + k);
                        tmpVector.push_back(tmpString);
                    }
                }
                answer.swap(tmpVector);
            }
        }
        return answer;
    }
};