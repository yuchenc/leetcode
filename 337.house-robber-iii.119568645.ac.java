/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int rob(TreeNode root) {
        int[] res = new int[2];
        res = getResult(root);
        return Math.max(res[0], res[1]);
    }
    public int[] getResult(TreeNode node)
    {
        int[] left = new int[2];
        int[] right = new int[2];
        int[] res = new int[2];
        if (node == null)
            return res;
        left = getResult(node.left);
        right = getResult(node.right);
        res[0] = Math.max(left[1], left[0]) + Math.max(right[1], right[0]);
        res[1] = left[0] + right[0] + node.val;
        return res;
    }
}