class Solution {
    public int wiggleMaxLength(int[] nums) {
        if (nums.length == 0)
            return 0;
        int answer = 1;
        int flag = -1;
        for (int i = 0; i < nums.length - 1; i++)
        {
            if (nums[i+1] > nums[i])
            {
                if (flag != 1)
                {
                    answer++;
                    flag = 1;
                }
            }
            else if (nums[i+1] < nums[i])
            {
                if (flag != 0)
                {
                    answer++;
                    flag = 0;
                }
            }
        }
        return answer;
    }
}