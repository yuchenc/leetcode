class Solution {
    public int calPoints(String[] ops) {
        Stack<Integer> stack = new Stack<>();
        int sum = 0;
        int prev = 0;
        stack.push(0);
        stack.push(0);
        for (int i = 0; i < ops.length; i++)
        {
            String s = ops[i];
            if (s.equals("C"))
            {
                sum -= stack.peek();
                stack.pop();
                int tmp = stack.pop();
                prev = stack.peek();
                stack.push(tmp);
            }
            else if (s.equals("D"))
            {
                int num = stack.peek() * 2;
                sum += num;
                prev = stack.peek();
                stack.push(num);
            }
            else if (s.equals("+"))
            {
                int num = prev + stack.peek();
                prev = stack.peek();
                sum += num;
                stack.push(num);
            }
            else
            {
                int num = Integer.parseInt(s);
                prev = stack.peek();
                stack.push(num);
                sum += num;
            }
        }
        return sum;
    }
}