/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s == null)
            return false;
        if (t == null)
            return true;
        return checkEqual(s, t) || isSubtree(s.left, t) || isSubtree(s.right, t);
    }
    public boolean checkEqual(TreeNode s, TreeNode t)
    {
        if (s == null && t == null)
            return true;
        else if (s!= null && t!= null)
            return s.val == t.val && checkEqual(s.left, t.left) && checkEqual(s.right, t.right);
        else
            return false;
    }
}