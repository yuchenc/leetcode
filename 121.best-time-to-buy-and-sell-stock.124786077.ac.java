class Solution {
    public int maxProfit(int[] prices) {
        if (prices.length <= 1)
            return 0;
        int current = 0;
        int answer = 0;
        for (int i = 1; i < prices.length; i++)
        {
            if (prices[i] - prices[i-1] + current >= 0)
            {
                answer = Math.max(answer, current + prices[i] - prices[i-1]);
                current += prices[i] - prices[i-1];
            }
            else
                current = 0;
        }
        return answer;
    }
}