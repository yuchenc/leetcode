class Solution {
    
    // I: 1; V: 5; X: 10; L: 50; C: 100; D: 500; M: 1000
    public int romanToInt(String s) {
        int answer = 0;
        if (s.length() == 0)
            return answer;
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        int prev = map.get(s.charAt(0));
        answer += prev;
        int current = 0;
        for (int i = 1; i < s.length(); i++)
        {
            current = map.get(s.charAt(i));
            if (current <= prev)
            {
                answer += current;
                prev = current;
            }
            else
            {
                answer += current - 2 * prev;
                prev = current;
            }
        }
        return answer;
        
    }
}