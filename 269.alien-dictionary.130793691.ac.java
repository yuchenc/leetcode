class Solution {
    public String alienOrder(String[] words) {
        boolean[][] graph = new boolean[26][26];
        int[] visit = new int[26];
        buildGraph(graph, visit, words);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 26; i++)
        {
            if (visit[i] == 0)
            {                 // unvisited
                if (!dfs(graph, visit, sb, i))
                    return "";
            }
        }
        return sb.reverse().toString();
    }
    
    private boolean dfs(boolean[][] graph, int[] visit, StringBuilder sb, int index) {
        visit[index] = 1;
        for (int i = 0; i < 26; i++)
        {
            if (graph[index][i])
            {
                if (visit[i] == 1)
                    return false;
                else if (visit[i] == 0)
                    if (!dfs(graph, visit, sb, i))
                        return false;
            }
        }
        visit[index] = 2;
        sb.append((char)(index + 'a'));
        return true;
    }
    
    private void buildGraph(boolean[][] graph, int[] visit, String[] words) {
        Arrays.fill(visit, -1);
        for (int i = 0; i < words.length; i++)
        {
            for (int j = 0; j < words[i].length(); j++)
                visit[words[i].charAt(j) - 'a'] = 0;
            if (i > 0)
            {
                String w1 = words[i-1];
                String w2 = words[i];
                for (int j = 0; j < Math.min(w1.length(), w2.length()); j++)
                {
                    if (w1.charAt(j) != w2.charAt(j))
                    {
                        graph[w1.charAt(j) - 'a'][w2.charAt(j) - 'a'] = true;
                        break;
                    }
                }
            }
        }
    }
}