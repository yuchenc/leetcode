class NumMatrix {
    private int[][] m;
    private int[][] sum;
    
    public NumMatrix(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0)
            return;
        this.m = matrix;
        sum = new int[matrix.length+1][matrix[0].length];
        for (int i = 1; i <= matrix.length; i++)
            for (int j = 0; j < matrix[0].length; j++)
                sum[i][j] = sum[i-1][j] + m[i-1][j];
    }
    
    public void update(int row, int col, int val) {
        for (int i = row + 1; i <= m.length; i++)
            sum[i][col] = sum[i][col] + val - m[row][col];
        m[row][col] = val;
    }
    
    public int sumRegion(int row1, int col1, int row2, int col2) {
        int res = 0;
        for (int i = col1; i <= col2; i++)
            res += sum[row2+1][i] - sum[row1][i];
        return res;
    }
}

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix obj = new NumMatrix(matrix);
 * obj.update(row,col,val);
 * int param_2 = obj.sumRegion(row1,col1,row2,col2);
 */