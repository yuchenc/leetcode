class Solution {
    public boolean isValid(String s) {
        Stack<Character> current = new Stack<Character>();
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == '(' || s.charAt(i) == '{' || s.charAt(i) == '[')
                current.push(s.charAt(i));
            else
            {
                if (!current.empty() && s.charAt(i) == ')' && current.peek() == '(')
                    current.pop();
                else if (!current.empty() && s.charAt(i) == ']' && current.peek() == '[')
                    current.pop();
                else if (!current.empty() && s.charAt(i) == '}' && current.peek() == '{')
                    current.pop();
                else
                    return false;
            }
        }
        if (current.empty())
            return true;
        else
            return false;
    }
}