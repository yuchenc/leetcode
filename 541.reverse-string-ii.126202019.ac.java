class Solution {
    public String reverseStr(String s, int k) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        int par = 2 * k;
        for (int i = 0; i < s.length(); i++)
        {
            int remain = i % par;
            if (remain < k)
                sb2.append(s.charAt(i));
            else if (remain == k)
            {
                sb.append(sb2.reverse());
                sb2 = new StringBuilder();
                sb.append(s.charAt(i));
            }
            else
                sb.append(s.charAt(i));
        }
        sb.append(sb2.reverse());
        return sb.toString();
        
    }
}