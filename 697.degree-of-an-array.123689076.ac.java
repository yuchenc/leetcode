class Solution {
    public int findShortestSubArray(int[] nums) {
        int max = 0;
        int answer = Integer.MAX_VALUE;
        HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++)
        {
            int num = nums[i];
            int size;
            if (map.containsKey(num))
            {
                ArrayList<Integer> tmp = map.get(num);
                tmp.add(i);
                size = tmp.size();
            }
            else
            {
                ArrayList<Integer> tmp = new ArrayList<>();
                tmp.add(i);
                map.put(num, tmp);
                size = tmp.size();
            }
            max = Math.max(size, max);  
        }
        for (Integer i : map.keySet())
        {
            ArrayList<Integer> tmp = map.get(i);
            if (tmp.size() == max)
                answer = Math.min(tmp.get(tmp.size() - 1) - tmp.get(0) + 1, answer);
        }
        return answer;
    }
}