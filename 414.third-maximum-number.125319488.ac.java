class Solution {
    public int thirdMax(int[] nums) {
        int max = 0;
        int max2 = 0;
        int max3 = 0;
        boolean flag1 = true;
        boolean flag2 = true;
        boolean flag3 = true;
        for (int num : nums)
        {
            if (flag1)
            {
                max = num;
                flag1 = false;
            }
            else if (flag2)
            {
                if (num > max)
                {
                    max2 = max;
                    max = num;
                    flag2 = false;
                }
                else if (num < max)
                {
                    max2 = num;
                    flag2 = false;
                }
            }
            else if (flag3)
            {
                if (num > max)
                {
                    max3 = max2;
                    max2 = max;
                    max = num;
                    flag3 = false;
                }
                else if (num < max && num > max2)
                {
                    max3 = max2;
                    max2 = num;
                    flag3 = false;
                }
                else if (num < max2)
                {
                    max3 = num;
                    flag3 = false;
                }
            }
            else if (num > max)
            {
                max3 = max2;
                max2 = max;
                max = num;
            }
            else if (num < max && num > max2)
            {
                max3 = max2;
                max2 = num;
            }
            else if (num > max3 && num < max2)
            {
                max3 = num;
            }
        }
        if (flag3)
            return max;
        else 
            return max3;
    }
}