/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
class Solution {
    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> answer = new ArrayList<>();
        int flag = 0;
        for (int i = 0; i < intervals.size(); i++)
        {
            Interval one = intervals.get(i);
            if (one.end < newInterval.start)
                answer.add(one);
            else if (one.start > newInterval.end)
            {
                if (flag == 0)
                {
                    answer.add(newInterval);
                    flag = 1;
                }
                answer.add(one);
            }
            else
            {
                newInterval.start = Math.min(newInterval.start, one.start);
                newInterval.end = Math.max(newInterval.end, one.end);
            }
        }
        if (flag == 0)
            answer.add(newInterval);
        return answer;
    }
}