class Solution {
    public int largestRectangleArea(int[] heights) {
        int columns=heights.length;
        int[] left=new int[columns];
        int[] right=new int[columns];
        for (int i=0; i<columns; i++){
            int x=i-1;
            while (x>=0&&heights[x]>=heights[i]){
                x=left[x];
            }
            left[i]=x;
        }
        for (int i=columns-1; i>=0; i--){
            int x=i+1;
            while (x<columns&&heights[x]>=heights[i]){
                x=right[x];
            }
            right[i]=x;
        }
        int max=0;
        for (int i=0; i<columns; i++){
            max=Math.max(max, heights[i]*(right[i]-left[i]-1));
        }
        return max;
    }
}