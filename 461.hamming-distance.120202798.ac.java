class Solution {
    public int hammingDistance(int x, int y) {
        int answer = 0;
        int val = x ^ y;
        while (val != 0)
        {
            answer += (val & 1);
            val = val >> 1;
        }
        return answer;
    }
}