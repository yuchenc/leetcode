/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean isValidBST(TreeNode root) {
        List<Integer> result1 = new ArrayList<>();
        List<Integer> result2 = new ArrayList<>();
        getResult(root, result1);
        getResult(root, result2);
        Collections.sort(result2);
        if (result2.size() != new HashSet<Integer>(result2).size())
            return false;
        return result1.equals(result2);
    }
    
    public void getResult(TreeNode node, List<Integer> result)
    {
        if (node == null)
            return;
        getResult(node.left, result);
        result.add(node.val);
        getResult(node.right, result);
    }
}
