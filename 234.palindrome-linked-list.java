/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean isPalindrome(ListNode head) {
        ListNode first = head;
        ListNode second = head;
        while (second != null && second.next != null)
        {
            first = first.next;
            second = second.next.next;
        }
        if (second != null)
            first = first.next;
        ListNode reverse = null;
        while (first != null)
        {
            ListNode tmp = first.next;
            first.next = reverse;
            reverse = first;
            first = tmp;
        }
        second = head;
        while (reverse != null)
        {
            if (reverse.val != second.val)
                return false;
            reverse = reverse.next;
            second = second.next;
        }
        return true;
    }
}
