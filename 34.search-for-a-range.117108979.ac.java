class Solution {
    public int[] searchRange(int[] nums, int target) {
        int[] answer = new int[2];
        int left = 0;
        int right = nums.length - 1;
        while (left <= right)
        {
            int middle = (left + right) / 2;
            if (nums[middle] == target)
            {
                int prev = middle;
                while (prev >= 0 && nums[prev] == target)
                    prev--;
                answer[0] = prev + 1;
                int back = middle;
                while (back <= nums.length - 1 && nums[back] == target)
                    back++;
                answer[1] = back - 1;
                return answer;
            }
            if (nums[middle] > target)
                right = middle - 1;
            else
                left = middle + 1;
        }
        answer[0] = -1;
        answer[1] = -1;
        return answer;
    }
}