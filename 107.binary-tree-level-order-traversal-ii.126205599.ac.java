/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        search(root, 0, res);
        List<List<Integer>> answer = new ArrayList<>();
        for (int i = 0; i < res.size(); i++)
            answer.add(res.get(res.size() - 1 - i));
        return answer;
    }
    
    public void search(TreeNode root, int level, List<List<Integer>> res)
    {
        if (root == null)
            return;
        if (res.size() <= level)
            res.add(new ArrayList<Integer>());
        res.get(level).add(root.val);
        search(root.left, level + 1, res);
        search(root.right, level + 1, res);
    }
}