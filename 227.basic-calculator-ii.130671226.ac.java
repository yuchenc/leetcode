class Solution {
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<>();
        char sig = '+';
        int number = 0;
        for (int i = 0; i < s.length(); i++)
        {
            if (Character.isDigit(s.charAt(i)))
            {
                number = number * 10 + (int)(s.charAt(i) - '0');
                //System.out.println(number);
            }
            if ((!Character.isDigit(s.charAt(i)) && s.charAt(i) != ' ') || i == s.length() - 1)
            {
                //System.out.println("DEBUG: " + number);
                if (sig == '+')
                    stack.push(number);
                else if (sig == '-')
                    stack.push(-number);
                else if (sig == '*')
                    stack.push(number * stack.pop());
                else if (sig == '/')
                    stack.push(stack.pop() / number);
                sig = s.charAt(i);
                number = 0;
            }
        }
        int res = 0;
        for (int n : stack)
            res += n;
        return res;
    }
}