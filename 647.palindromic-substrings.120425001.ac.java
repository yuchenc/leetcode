class Solution {
    public int countSubstrings(String s) {
        int answer = 0;
        int len = s.length();
        for (int i = 0; i < len; i++)
        {
            answer++;
            int left = i - 1;
            int right = i + 1;
            while (left >= 0 && right < len && s.charAt(left) == s.charAt(right))
            {
                answer++;
                left--;
                right++;
            }
        }
        for (int i = 1; i < len; i++)
        {
            if (s.charAt(i-1) == s.charAt(i))
            {
                answer++;
                int left = i - 2;
                int right = i + 1;
                while (left >= 0 && right < len && s.charAt(left) == s.charAt(right))
                {
                    answer++;
                    left--;
                    right++;
                }
            }
        }
        return answer;
    }
}