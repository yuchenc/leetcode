class Solution {
    public int trailingZeroes(int n) {
        int answer = 0;
        long a = 5;
        while (a <= n)
        {
            answer += n / a;
            a = a * 5;
        }
        return answer;
    }
}