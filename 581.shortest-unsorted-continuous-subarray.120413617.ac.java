class Solution {
    public int findUnsortedSubarray(int[] nums) {
        int answer = nums.length;
        int[] copy = new int[answer];
        for (int i = 0; i < answer; i++)
            copy[i] = nums[i];
        Arrays.sort(copy);
        int index = 0;
        while (index < nums.length && nums[index] == copy[index])
        {
            answer--;
            index++;
        }
        if (answer == 0)
            return answer;
        index = nums.length - 1;
        while (nums[index] == copy[index])
        {
            answer--;
            index--;
        }
        return answer;
    }
}