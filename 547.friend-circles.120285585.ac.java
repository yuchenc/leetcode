class Solution {
    public int findCircleNum(int[][] M) {
        int answer = 0;
        int n = M.length;
        if (n == 0)
            return 0;
        int[] visited = new int[n];
        for (int i = 0; i < n; i++)
        {
            if (visited[i] == 0)
            {
                search(M, visited, i);
                answer++;
            }
        }
        return answer;
    }
    public void search(int[][] M, int[] visited, int row)
    {
        for (int j = 0; j < M.length; j++)
        {
            if (M[row][j] == 1 && visited[j] == 0)
            {
                visited[j] = 1;
                search(M, visited, j);
            }
        }
    }
}