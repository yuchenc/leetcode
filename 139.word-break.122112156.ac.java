class Solution {
    public boolean wordBreak(String s, List<String> wordDict) {
       boolean[] flag = new boolean[s.length()+1];
        
        flag[0] = true;
        int len = s.length();
        for(int i = 1; i <= len; i++)
            for(int j = 0 ; j < i; j++){
                if(flag[j] && wordDict.contains(s.substring(j,i)))
                    flag[i] = true;
            }
        
        return flag[len];
    }
}