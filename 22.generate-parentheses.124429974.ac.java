class Solution {
    public List<String> generateParenthesis(int n) {
        List<List<String>> dp = new ArrayList<>();
        List<String> list = new ArrayList<>();
        list.add("");
        dp.add(list);
        for (int i = 1; i <= n; i++)
        {
            list = new ArrayList<>();
            for (int j = 0; j < i; j++)
            {
                for (String s1 : dp.get(j))
                {
                    for (String s2 : dp.get(i-1-j))
                    {
                        list.add("(" + s1 + ")" + s2);
                    }
                }
            }
            dp.add(list);
        }
        return dp.get(n);
    }
}