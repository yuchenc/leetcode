class Solution {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        HashSet<String> wordLists = new HashSet<>();
        for (String str : wordList)
            wordLists.add(str);
        HashSet<String> visited = new HashSet<>();
        if (beginWord.length() != endWord.length())
            return 0;
        if (!wordLists.contains(endWord))
            return 0;
        HashSet<String> current = new HashSet<>();
        current.add(beginWord);
        int length = 1;
        while (!current.isEmpty())
        {
            HashSet<String> tmp = new HashSet<>();
            for (String str : current)
            {
                System.out.println(str);
                char[] chars = str.toCharArray();
                for (int i = 0; i < chars.length; i++)
                {
                    for (char j = 'a'; j <= 'z'; j++)
                    {
                        char c = chars[i];
                        chars[i] = j;
                        String target = String.valueOf(chars);
                        if (target.equals(endWord))
                            return length+1;
                        if (!visited.contains(target) && wordLists.contains(target))
                        {
                            tmp.add(target);
                            visited.add(target);
                        }
                        chars[i] = c;
                    }
                }
            }
            length++;
            current = tmp;
        }
        return 0;
    }
}