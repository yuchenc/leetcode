/*
// Employee info
class Employee {
    // It's the unique id of each node;
    // unique id of this employee
    public int id;
    // the importance value of this employee
    public int importance;
    // the id of direct subordinates
    public List<Integer> subordinates;
};
*/
class Solution {
    public int getImportance(List<Employee> employees, int id) {
        HashMap<Integer, Employee> map = new HashMap<>();
        for (Employee one : employees)
            map.put(one.id, one);
        return getImportance(map, id);
    }
    
    public int getImportance(HashMap<Integer, Employee> map, int id)
    {
        int answer = 0;
        Employee one = map.get(id);
        answer += one.importance;
        for (int newid : one.subordinates)
            answer += getImportance(map, newid);
        return answer;
    }
}