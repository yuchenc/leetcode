class Solution {
    public boolean wordBreak(String s, List<String> wordDict) {
        HashSet<String> set = new HashSet<>();
        for (String word : wordDict)
            set.add(word);
        boolean[] status = new boolean[s.length()+1];
        status[0] = true;
        for (int i = 1; i <= s.length(); i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (status[j])
                {
                    String sub = s.substring(j,i);
                    if (set.contains(sub))
                    {
                        status[i] = true;
                        break;
                    }
                }
            }
        }
        return status[s.length()];
    }
}