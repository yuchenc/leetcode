class Solution {
    public String reverseVowels(String s) {
        char[] chars = s.toCharArray();
        int left = 0;
        int right = chars.length - 1;
        HashSet<Character> set = new HashSet<>();
        set.add('a');
        set.add('o');
        set.add('e');
        set.add('i');
        set.add('u');
        set.add('A');
        set.add('O');
        set.add('E');
        set.add('I');
        set.add('U');
        while (left < right)
        {
            if (set.contains(chars[left]) && set.contains(chars[right]))
            {
                char tmp = chars[left];
                chars[left] = chars[right];
                chars[right] = tmp;
                left++;
                right--;
            }
            else if (set.contains(chars[left]) && !set.contains(chars[right]))
                right--;
            else if (!set.contains(chars[left]) && set.contains(chars[right]))
                left++;
            else
            {
                left++;
                right--;
            }
        }
        return String.valueOf(chars);
    }
}