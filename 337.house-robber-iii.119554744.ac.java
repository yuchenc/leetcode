/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int rob(TreeNode root) {
        Map<TreeNode, Integer> rob = new HashMap<>();
        Map<TreeNode, Integer> norob = new HashMap<>();
        rob.put(null, 0);
        norob.put(null, 0);
        getResult(root, rob, norob);
        return Math.max(rob.get(root), norob.get(root));
        
    }
    public void getResult(TreeNode node, Map<TreeNode, Integer> rob, Map<TreeNode, Integer> norob)
    {
        if (node == null)
            return;
        getResult(node.left, rob, norob);
        getResult(node.right, rob, norob);
        rob.put(node, node.val + norob.get(node.left) + norob.get(node.right));
        norob.put(node, Math.max(norob.get(node.left), rob.get(node.left)) + Math.max(norob.get(node.right), rob.get(node.right)));
    }
}