class RandomizedSet {
    List<Integer> locs;
    // key is the value, value is the location of the value in list
    HashMap<Integer, Integer> map;
    java.util.Random rand = new java.util.Random();
    /** Initialize your data structure here. */
    public RandomizedSet() {
        locs = new ArrayList<>();
        map = new HashMap<>();
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        if (map.containsKey(val))
            return false;
        else
        {
            map.put(val, locs.size());
            locs.add(val);
            return true;
        }
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        if (map.containsKey(val))
        {
            int location = map.get(val);
            if (location == locs.size() - 1)
            {
                locs.remove(locs.size() - 1);
                map.remove(val);
            }
            else
            {
                int tmp = locs.get(locs.size() - 1);
                locs.set(location, tmp);
                locs.remove(locs.size() - 1);
                map.remove(val);
                map.put(tmp, location);
            }
            return true;
        }
        else
            return false;
    }
    
    /** Get a random element from the set. */
    public int getRandom() {
        int location = rand.nextInt(locs.size());
        return locs.get(location);
    }
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */