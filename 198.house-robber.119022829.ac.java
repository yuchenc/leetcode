class Solution {
    public int rob(int[] nums) {
        if (nums.length == 0)
            return 0;
        if (nums.length == 1)
            return nums[0];
        int[] answer = new int[nums.length];
        answer[0] = nums[0];
        answer[1] = nums[1];
        int result = Math.max(answer[0], answer[1]);
        if (nums.length == 2)
            return result;
        answer[2] = nums[0] + nums[2];
        result = Math.max(result, answer[2]);
        for (int i = 3; i < nums.length; i++)
        {
            answer[i] = Math.max(nums[i] + answer[i-2], nums[i] + answer[i-3]);
            if (answer[i] > result)
                result = answer[i];
        }
        return result;
    }
}