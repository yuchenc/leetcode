class Solution {
    public int[] countBits(int num) {
        int[] answer = new int[num + 1];
        for (int i = 1; i <= num; i++)
            answer[i] = answer[i >> 1] + (i & 1);
        return answer;
    }
}