class Solution {
    public List<List<String>> groupStrings(String[] strings) {
        List<List<String>> answer = new ArrayList<>();
        HashMap<String, List<String>> map = new HashMap<>();
        for (String str : strings)
        {
            String origin = get(str);
            if (map.containsKey(origin))
            {
                List<String> list = map.get(origin);
                list.add(str);
            }
            else
            {
                List<String> list = new ArrayList<>();
                list.add(str);
                map.put(origin, list);
                answer.add(list);
            }
        }
        return answer;
    }
    
    public String get(String str) {
        if (str.length() == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        sb.append('a');
        int diff = str.charAt(0) - 'a';
        for (int i = 1; i < str.length(); i++)
        {
            char c = (char)(str.charAt(i) - diff);
            if (c < 'a')
                c += 26;
            sb.append(c);
        }
        return sb.toString();
    }
}