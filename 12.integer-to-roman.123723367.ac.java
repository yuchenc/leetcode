class Solution {
    public String intToRoman(int num) {
        String answer = "";
        if (num == 0)
            return answer;
        int tag = num / 1000;
        if (tag > 0)
        {
            for (int i = 0; i < tag; i++)
                answer += "M";
            answer += intToRoman(num%1000);
            return answer;
        }
        if (num >= 900)
        {
            answer += "CM";
            answer += intToRoman(num-900);
            return answer;
        }
        tag = num / 500;
        if (tag > 0)
        {
            answer += "D";
            answer += intToRoman(num%500);
            return answer;
        }
        if (num >= 400)
        {
            answer += "CD";
            answer += intToRoman(num-400);
            return answer;
        }
        tag = num / 100;
        if (tag > 0)
        {
            for (int i = 0; i < tag; i++)
                answer += "C";
            answer += intToRoman(num%100);
            return answer;
        }
        if (num >= 90)
        {
            answer += "XC";
            answer += intToRoman(num-90);
            return answer;
        }
        tag = num / 50;
        if (tag > 0)
        {
            answer += "L";
            answer += intToRoman(num%50);
            return answer;
        }
        if (num >= 40)
        {
            answer += "XL";
            answer += intToRoman(num-40);
            return answer;
        }
        tag = num / 10;
        if (tag > 0)
        {
            for (int i = 0; i < tag; i++)
                answer += "X";
            answer += intToRoman(num%10);
            return answer;
        }
        if (num >= 9)
        {
            answer += "IX";
            answer += intToRoman(num-9);
            return answer;
        }
        tag = num / 5;
        if (tag > 0)
        {
            answer += "V";
            answer += intToRoman(num%5);
            return answer;
        }
        if (num >= 4)
        {
            answer += "IV";
            answer += intToRoman(num-4);
            return answer;
        }
        tag = num / 1;
        if (tag > 0)
        {
            for (int i = 0; i < tag; i++)
                answer += "I";
            answer += intToRoman(num%1);
            return answer;
        }
        return answer;
    }
}