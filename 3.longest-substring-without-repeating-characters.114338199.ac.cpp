class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        if (s == "")
            return 0;
        string answer;
        string back;
        answer.push_back(s[0]);
        back.push_back(s[0]);
        for (int i = 1; i < s.length(); i++)
        {
            if(back.find(s[i]) != std::string::npos)
            {
                back = back.substr(back.find(s[i]) + 1);
                back.push_back(s[i]);
            }
            else
                back.push_back(s[i]);
            if (back.length() >= answer.length())
            {
                answer = back;
            }
        }
        return answer.length();  
    }
};