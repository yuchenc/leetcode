/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean findTarget(TreeNode root, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        goThrough(root, map);
        return goFind(root, map, k);
    }
    
    public void goThrough(TreeNode root, HashMap<Integer, Integer> map) {
        if (root == null)
            return;
        map.put(root.val, map.getOrDefault(root.val, 0) + 1);
        goThrough(root.left, map);
        goThrough(root.right, map);
    }
    
    public boolean goFind(TreeNode root, HashMap<Integer, Integer> map, int target) {
        if (root == null)
            return false;
        if (map.containsKey(target - root.val))
        {
            if (root.val == target - root.val && map.get(target - root.val) > 1)
                return true;
            else if (root.val != target - root.val)
                return true;
        }
        return goFind(root.left, map, target) || goFind(root.right, map, target);
    }
}