class Solution {
    public int arrayPairSum(int[] nums) {
        int answer = 0;
        int[] t = new int[20001];
        for (int i = 0; i < nums.length; i++)
            t[nums[i]+10000]++;
        int flag = 0;
        for (int i = 20000; i >= 0;)
        {
            if (t[i] > 0)
            {
                if (flag == 0)
                {
                    flag = 1;
                    t[i]--;
                }
                else
                {
                    flag = 0;
                    answer += i - 10000;
                    t[i]--;
                }
            }
            else
                i--;
        }
        return answer;
    }
}