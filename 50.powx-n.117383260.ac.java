class Solution {
    public double myPow(double x, int n) {
        if (x == 1)
            return 1;
        if (n == Integer.MIN_VALUE)
            return 1/x/myPow(x, n - 1);
        if (n < 0)
            return 1/myPow(x, -n);
        else if (n == 0)
            return 1;
        else if (n == 1)
            return x;
        else
        {
            if (n % 2 == 1)
            {
                double middle = myPow(x, n / 2);
                return x * middle * middle;
            }
            else
            {
                double middle = myPow(x, n / 2);
                return middle * middle;
            }
        }
    }
}