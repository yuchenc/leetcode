class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> result = new HashMap<>();
        for (int i = 0; i < strs.length; i++)
        {
            char[] tmp = strs[i].toCharArray();
            Arrays.sort(tmp);
            String tmpString = String.valueOf(tmp);
            if (result.containsKey(tmpString))
            {
                result.get(tmpString).add(strs[i]);
            }
            else
            {
                List<String> tmpList = new ArrayList<>();
                tmpList.add(strs[i]);
                result.put(tmpString, tmpList);
            }
        }
        return new ArrayList<List<String>>(result.values());
    }
}