class Solution {
    public int[] constructRectangle(int area) {
        int i = (int)Math.sqrt(area);
        for (; i > 0; i--)
        {
            if (area % i == 0)
                break;
        }
        int[] res = new int[2];
        res[0] = area / i;
        res[1] = i;
        return res;
    }
}