class Solution {
    public int minSubArrayLen(int s, int[] nums) {
        int left = 0;
        int right = 0;
        int sum = 0;
        int min = Integer.MAX_VALUE;
        while (right < nums.length) {
            sum += nums[right];
            right++;
            while (sum >= s) {
                if (right - left < min) {
                    min = right - left;
                }
                sum -= nums[left];
                left++;
            }
        }
        if (min == Integer.MAX_VALUE) {
            return 0;
        }
        else {
            return min;
        }
    }
}