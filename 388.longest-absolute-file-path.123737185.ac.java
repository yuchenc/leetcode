class Solution {
    public int lengthLongestPath(String input) {
        Stack<String> stack = new Stack<>();
        String[] parts = input.split("\n");
        int max = 0;
        int sum = 0;
        for (int i = 0; i < parts.length; i++)
        {
            int level = 0;
            int j = 0;
            for (; j < parts[i].length(); j++)
            {
                if (parts[i].charAt(j) == '\t')
                    level++;
                else
                    break;
            }
            String filename = parts[i].substring(j);
            int size = stack.size();
            for (int k = level; k < size; k++)
                sum -= stack.pop().length();
            sum += filename.length();
            stack.push(filename);
            if (filename.indexOf(".") > 0)
                max = Math.max(max, sum + stack.size() - 1);
        }
        return max;
    }
}