class Solution {
    public int[] countBits(int num) {
        int[] answer = new int[num + 1];
        answer[0] = 0;
        int pow = 0;
        for (int i = 1; i <= num; i++)
        {
            if ((i & i-1) == 0)
            {
                answer[i] = 1;
                pow = i;
            }
            else
                answer[i] = 1 + answer[i - pow];
        }
        return answer;
    }
}