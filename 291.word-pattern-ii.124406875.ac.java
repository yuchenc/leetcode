class Solution {
    public boolean wordPatternMatch(String pattern, String str) {
        HashMap<Character, String> map = new HashMap<>();
        HashSet<String> set = new HashSet<>();
        return isMatch(pattern, 0, str, 0, map, set);
    }
    
    public boolean isMatch(String pattern, int i, String str, int j, HashMap<Character, String> map, HashSet<String> set) {
        if (i == pattern.length() && j == str.length())
            return true;
        if (i == pattern.length() || j == str.length())
            return false;
        char c = pattern.charAt(i);
        if (map.containsKey(c))
        {
            String s = map.get(c);
            if (!str.startsWith(s, j))
                return false;
            else
                return isMatch(pattern, i+1, str, j+s.length(), map, set);
        }
        else
        {
            for (int k = j; k < str.length(); k++)
            {
                String s = str.substring(j, k+1);
                if (set.contains(s))
                    continue;
                else
                {
                    map.put(c, s);
                    set.add(s);
                    if (isMatch(pattern, i+1, str, j+s.length(), map, set))
                        return true;
                    else
                    {
                        map.remove(c);
                        set.remove(s);
                    }
                }
            }
        }
        return false;
    }
}