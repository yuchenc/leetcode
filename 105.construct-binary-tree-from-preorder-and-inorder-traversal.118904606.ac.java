/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length == 0)
            return null;
        TreeNode root = new TreeNode(preorder[0]);
        int i = 0;
        for (; i < inorder.length; i++)
            if (inorder[i] == preorder[0])
                break;
        root.left = buildTree(preorder, inorder, 1, i, 0, i - 1);
        root.right = buildTree(preorder, inorder, i + 1, preorder.length - 1, i + 1, inorder.length - 1);
        return root;
        
    }
    
    public TreeNode buildTree(int[] preorder, int[] inorder, int prestart, int preend, int instart, int inend)
    {
        if (prestart > preend)
            return null;
        TreeNode node = new TreeNode(preorder[prestart]);
        if (prestart == preend)
            return node;
        int index = 0;
        for (; index <= inend; index++)
            if (preorder[prestart] == inorder[index])
                break;
        node.left = buildTree(preorder, inorder, prestart + 1, index - instart + prestart, instart, index - 1);
        node.right = buildTree(preorder, inorder, prestart + index - instart + 1, preend, index + 1, inend);
        return node;
    }
}