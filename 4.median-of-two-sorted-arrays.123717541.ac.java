class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int len1 = nums1.length;
        int len2 = nums2.length;
        if (len1 + len2 == 0)
            return 0;
        if (len2 < len1)
            return findMedianSortedArrays(nums2, nums1);
        if (len1 == 0)
        {
            if (len2 % 2 == 1)
                return nums2[len2/2]*1.0;
            else
                return (nums2[len2/2-1] + nums2[len2/2])*1.0/2;
        }
        
        int left = 0;
        int right = len1;
        int leftcut = (left + right)/2;
        int rightcut = (len1 + len2 + 1)/2 - leftcut;
        
        int leftmax = 0;
        if (leftcut == 0)
            leftmax = nums2[rightcut - 1];
        else if (rightcut == 0)
            leftmax = nums1[leftcut - 1];
        else
            leftmax = Math.max(nums2[rightcut - 1], nums1[leftcut - 1]);
        int rightmin = 0;
        if (leftcut == len1)
            rightmin = nums2[rightcut];
        else if (rightcut == len2)
            rightmin = nums1[leftcut];
        else
            rightmin = Math.min(nums2[rightcut], nums1[leftcut]);
        while (leftmax > rightmin)
        {
            if (leftcut == 0)
                left = leftcut + 1;
            else if (leftcut == len1)
                right = leftcut - 1;
            else if (nums1[leftcut - 1] < nums2[rightcut - 1])
                left = leftcut + 1;
            else
                right = leftcut - 1;
            leftcut = (left + right)/2;
            rightcut = (len1 + len2 + 1)/2 - leftcut;
            if (leftcut == 0)
                leftmax = nums2[rightcut - 1];
            else if (rightcut == 0)
                leftmax = nums1[leftcut - 1];
            else
                leftmax = Math.max(nums2[rightcut - 1], nums1[leftcut - 1]);
            if (leftcut == len1)
                rightmin = nums2[rightcut];
            else if (rightcut == len2)
                rightmin = nums1[leftcut];
            else
                rightmin = Math.min(nums2[rightcut], nums1[leftcut]);
        }
        if ((len1 + len2) % 2 == 0)
            return (leftmax + rightmin)*1.0/2;
        else
            return leftmax*1.0;
    }
}