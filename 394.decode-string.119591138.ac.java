class Solution {
    public String decodeString(String s) {
        Stack<Integer> count = new Stack<>();
        Stack<String> string = new Stack<>();
        String res = "";
        for (int i = 0; i < s.length();)
        {
            if (Character.isDigit(s.charAt(i)))
            {
                int number = 0;
                while (Character.isDigit(s.charAt(i)))
                {
                    number = number * 10 + (s.charAt(i) - '0');
                    i++;
                }
                count.push(number);
            }
            else if (s.charAt(i) == '[')
            {
                string.push(res);
                res = "";
                i++;
            }
            else if (s.charAt(i) == ']')
            {
                String pop = string.pop();
                int repeat = count.pop();
                for (int j = 0; j < repeat; j++)
                    pop += res;
                res = pop;
                i++;
            }
            else
                res += s.charAt(i++);
        }
        return res;
    }
}