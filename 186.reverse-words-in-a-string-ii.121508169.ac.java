public class Solution {
    public void reverseWords(char[] s) {
        for (int i = 0; i < s.length/2; i++)
        {
            char tmp = s[i];
            s[i] = s[s.length - 1 - i];
            s[s.length - 1 - i] = tmp;
        }
        for (int i = 0; i < s.length; i++)
        {
            int left = i;
            while (i < s.length && s[i] != ' ')
                i++;
            for (int j = left; j < (i+left)/2; j++)
            {
                char tmp = s[j];
                s[j] = s[i-1-j+left];
                s[i-1-j+left] = tmp;
            }
        }
    }
}