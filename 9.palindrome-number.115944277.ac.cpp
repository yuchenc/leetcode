class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0)
            return false;
        int p = x % 10;
        int q = x / 10;
        int answer = p;
        int newanswer = answer;
        while (q != 0)
        {
            p = q % 10;
            q = q / 10;
            newanswer = answer * 10 + p;
            if (newanswer / 10 != answer)
                return false;
            answer = newanswer;
        }
        if (answer == x)
            return true;
        else
            return false;
        
    }
};