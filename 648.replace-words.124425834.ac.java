class Solution {
    public String replaceWords(List<String> dict, String sentence) {
        StringBuilder sb = new StringBuilder();
        String[] words = sentence.split(" ");
        for (int i = 0; i < words.length; i++)
        {
            for (String str : dict)
            {
                if (str.length() < words[i].length() && words[i].startsWith(str))
                    words[i] = str;
            }
        }
        for (String word : words)
            sb.append(word + " ");
        String answer = sb.toString();
        return answer.substring(0, answer.length()-1);
    }
}