class Solution {
    public int numDistinctIslands(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0)
            return 0;
        HashSet<String> set = new HashSet<>();
        for (int i = 0; i < grid.length; i++)
        {
            for (int j = 0; j < grid[0].length; j++)
            {
                if (grid[i][j] == 1)
                    set.add(dfs(grid, i, j));
            }
        }
        return set.size();
    }
    public String dfs(int[][] grid, int i, int j)
    {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length)
            return "N";
        if (grid[i][j] == 0)
            return "N";
        String res = "A";
        grid[i][j] = 0;
        res += dfs(grid, i-1, j);
        res += dfs(grid, i, j-1);
        res += dfs(grid, i+1, j);
        res += dfs(grid, i, j+1);
        return res;
    }
}