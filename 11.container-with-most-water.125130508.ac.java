class Solution {
    public int maxArea(int[] height) {
        int res = 0;
        int left = 0;
        int right = height.length - 1;
        while (left < right)
        {
            if (height[left] < height[right])
            {
                res = Math.max(res, height[left] * (right - left));
                left++;
            }
            else
            {
                res = Math.max(res, height[right] * (right - left));
                right--;
            }
        }
        return res;
    }
}