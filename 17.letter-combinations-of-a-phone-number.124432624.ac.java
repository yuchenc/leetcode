class Solution {
    public List<String> letterCombinations(String digits) {
        List<String> answer = new ArrayList<>();
        if (digits.length() == 0)
            return answer;
        List<List<Character>> dict = new ArrayList<>();
        for (int i = 2; i <= 6; i++)
        {
            List<Character> list = new ArrayList<>();
            list.add((char)('a' + 3 * (i-2) + 0));
            list.add((char)('a' + 3 * (i-2) + 1));
            list.add((char)('a' + 3 * (i-2) + 2));
            dict.add(list);
        }
        List<Character> list = new ArrayList<>(Arrays.asList('p','q','r','s'));
        dict.add(list);
        list = new ArrayList<>(Arrays.asList('t', 'u', 'v'));
        dict.add(list);
        list = new ArrayList<>(Arrays.asList('w','x','y','z'));
        dict.add(list);
        return recursion(digits, dict, 0);
    }
    
    public List<String> recursion(String digits, List<List<Character>> dict, int index) {
        List<String> res = new ArrayList<>();
        if (index == digits.length())
        {
            res.add("");
            return res;
        }
        char d = digits.charAt(index);
        if (d >= '2' && d <= '9')
        {
            for (Character c : dict.get(d - '2'))
            {
                for (String str : recursion(digits, dict, index + 1))
                {
                    res.add(c + str);
                }
            }
        }
        return res;
    }
}