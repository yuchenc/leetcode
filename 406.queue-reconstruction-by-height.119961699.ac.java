class Solution {
    public int[][] reconstructQueue(int[][] people) {
        Arrays.sort(people, new Comparator<int[]>() {  
            public int compare(int[] left, int[] right)
            {
                if (left[0] == right[0])
                    return left[1] - right[1];
                else
                    return right[0] - left[0];
            }
        });
        for (int i = 1; i < people.length; i++)
        {
            if (people[i][0] != people[i-1][0])
            {
                int[] tmp = people[i];
                int pos = people[i][1];
                if (pos > i)
                    return null; // error
                else
                {
                    int index = i;
                    while (pos != index)
                    {
                        people[index] = people[index-1];
                        index--;
                    }
                    people[pos] = tmp;
                }
            }
        }
        return people;
    }
}