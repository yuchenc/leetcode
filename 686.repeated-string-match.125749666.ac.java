class Solution {
    public int repeatedStringMatch(String A, String B) {
        int repeat = 0;
        StringBuilder sb = new StringBuilder();
        while (sb.length() < B.length())
        {
            repeat++;
            sb.append(A);
        }
        if (sb.indexOf(B) >= 0)
            return repeat;
        else
        {
            repeat++;
            sb.append(A);
            if (sb.indexOf(B) >= 0)
                return repeat;
            else
                return -1;
        }
    }
}