/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public void flatten(TreeNode root) {
        if (root == null)
            return;
        flatten(root.left);
        flatten(root.right);     
        TreeNode end = root.left;
        while (end != null)
        {
            if (end.right == null)
            {
                end.right = root.right;
                break;
            }
            end = end.right;
        }
        if (root.left == null)
            return;
        root.right = root.left;
        root.left = null;
    }
}