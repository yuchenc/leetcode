class Solution {
    public String countAndSay(int n) {
        if (n == 1)
            return "1";
        String prev = "1";
        String current = "";
        for (int i = 2; i <= n; i++)
        {
            current = "";
            char tmp = prev.charAt(0);
            int count = 1;
            for (int j = 1; j < prev.length(); j++)
            {
                if (prev.charAt(j) == tmp)
                    count++;
                else
                {
                    current += String.valueOf(count);
                    current += tmp;
                    tmp = prev.charAt(j);
                    count = 1;
                }
            }
            current += String.valueOf(count);
            current += tmp;
            prev = current;
        }
        return current;
    }
}