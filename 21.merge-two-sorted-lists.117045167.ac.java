/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode answer = new ListNode(0);
        ListNode back = answer;
        while (l1 != null && l2 != null)
        {
            if (l1.val < l2.val)
            {
                ListNode tmp = new ListNode(l1.val);
                back.next = tmp;
                back = back.next;
                l1 = l1.next;
            }
            else
            {
                ListNode tmp = new ListNode(l2.val);
                back.next = tmp;
                back = back.next;
                l2 = l2.next;
            }
        }
        if (l1 == null)
            back.next = l2;
        else
            back.next = l1;
        return answer.next;
    }
}