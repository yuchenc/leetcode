class MapSum {
    class Trie {
        int num;
        Trie[] children;
        public Trie()
        {
            children = new Trie[26];
        }
    }

    Trie root;
    HashMap<String, Integer> map;
    /** Initialize your data structure here. */
    public MapSum() {
        root = new Trie();
        root.num = 0;
        map = new HashMap<>();
    }
    
    public void insert(String key, int val) {
        Trie current = root;
        for (int i = 0; i < key.length(); i++)
        {
            char c = key.charAt(i);
            int index = c - 'a';
            if (current.children[index] == null)
                current.children[index] = new Trie();
            current = current.children[index];
            if (map.containsKey(key))
            {
                current.num += val - map.get(key);
            }
            else
                current.num += val;
        }
        map.put(key, val);
    }
    
    public int sum(String prefix) {
        Trie current = root;
        for (int i = 0; i < prefix.length(); i++)
        {
            char c = prefix.charAt(i);
            int index = c - 'a';
            if (current.children[index] == null)
                return 0;
            current = current.children[index];
        }
        return current.num;
    }
}

/**
 * Your MapSum object will be instantiated and called as such:
 * MapSum obj = new MapSum();
 * obj.insert(key,val);
 * int param_2 = obj.sum(prefix);
 */