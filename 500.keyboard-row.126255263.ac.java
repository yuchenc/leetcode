class Solution {
    public String[] findWords(String[] words) {
        HashSet<Character> set1 = new HashSet<>(Arrays.asList('z','x','c','v','b','n','m'));
        HashSet<Character> set2 = new HashSet<>(Arrays.asList('a','s','d','f','g','h','j','k','l'));
        HashSet<Character> set3 = new HashSet<>(Arrays.asList('q','w','e','r','t','y','u','i','o','p'));
        List<String> res = new ArrayList<>();
        for (int i = 0; i < words.length; i++)
        {
            int flag = 0;
            String s = words[i];
            s = s.toLowerCase();
            boolean is = false;
            for (int j = 0; j < s.length(); j++)
            {
                if (flag == 0)
                {
                    if (set1.contains(s.charAt(j)))
                        flag = 1;
                    else if (set2.contains(s.charAt(j)))
                        flag = 2;
                    else
                        flag = 3;
                }
                else if (flag == 1)
                {
                    if (!set1.contains(s.charAt(j)))
                        break;
                }
                else if (flag == 2)
                {
                    if (!set2.contains(s.charAt(j)))
                        break;
                }
                else
                {
                    if (!set3.contains(s.charAt(j)))
                        break;
                }
                if (j == s.length() - 1)
                    is = true;
            }
            if (is)
                res.add(words[i]);
        }
        String[] answer = new String[res.size()];
        res.toArray(answer);
        return answer;
    }
}