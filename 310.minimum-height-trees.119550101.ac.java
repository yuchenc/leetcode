class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        if (n == 1) return Collections.singletonList(0);
        List<Set<Integer>> graph = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            graph.add(new HashSet<>());
        for (int i = 0; i < edges.length; i++)
        {
            graph.get(edges[i][0]).add(edges[i][1]);
            graph.get(edges[i][1]).add(edges[i][0]);
        }
        List<Integer> leaves = new ArrayList<>();
        for (int i = 0; i < graph.size(); i++)
        {
            if (graph.get(i).size() == 1)
                leaves.add(i);
        }
        while (n > 2)
        {
            n -= leaves.size();
            List<Integer> newleaves = new ArrayList<>();
            for (int leaf : leaves)
            {
                int next = graph.get(leaf).iterator().next();
                graph.get(next).remove(leaf);
                if (graph.get(next).size() == 1)
                    newleaves.add(next);
            }
            leaves = newleaves;
        }
        return leaves;
    }
}