class Solution {
    public void rotate(int[][] matrix) {
        int n = matrix.length;
        for (int level = 0; level < (n+1)/2; level++)
        {
            for (int i = level; i < n-level-1; i++)
                swap(matrix, level, i, i, n-level-1);
            for (int i = level; i < n-level-1; i++)
                swap(matrix, level, i, n-1-level, n-1-i);
            for (int i = level; i < n-level-1; i++)
                swap(matrix, level, i, n-1-i, level);
        }
    }
    
    public void swap(int[][] matrix, int x1, int y1, int x2, int y2) {
        int tmp = matrix[x1][y1];
        matrix[x1][y1] = matrix[x2][y2];
        matrix[x2][y2] = tmp;
    }
}