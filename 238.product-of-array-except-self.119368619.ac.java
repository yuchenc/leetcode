class Solution {
    public int[] productExceptSelf(int[] nums) {
        int[] answer = new int[nums.length];
        int sum = 1;
        int count = 0;
        int zero = -1;
        for (int i = 0; i < nums.length; i++)
        {
            if (nums[i] == 0)
            {
                count++;
                zero = i;
            }
            else
                sum *= nums[i];
        }
        if (count >= 2)
            return answer;
        else if (count == 1)
        {
            answer[zero] = sum;
            return answer;
        }
        else
        {
            for (int i = 0; i < nums.length; i++)
                answer[i] = sum / nums[i];
            return answer;
        }
        
    }
}