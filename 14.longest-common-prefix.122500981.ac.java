class Solution {
    public String longestCommonPrefix(String[] strs) {
        String res = "";
        if (strs.length == 0)
            return "";
        int index = 0;
        while (true)
        {
            int flag = 0;
            for (int i = 0; i < strs.length; i++)
            {
                if (index == strs[i].length())
                {
                    flag = 1;
                    break;
                }
                if (i > 0 && strs[i].charAt(index) != strs[i-1].charAt(index))
                {
                    flag = 1;
                    break;
                }
            }
            if (flag == 1)
                break;
            res += strs[0].charAt(index);
            index++;
        }
        return res;
    }
}