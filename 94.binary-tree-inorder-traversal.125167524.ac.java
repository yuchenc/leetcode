/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> answer = new ArrayList<>();
        TreeNode current = root;
        TreeNode prev = null;
        while (current != null)
        {
            if (current.left == null)
            {
                answer.add(current.val);
                current = current.right;
            }
            else
            {
                prev = current.left;
                while (prev.right != null && prev.right != current)
                    prev = prev.right;
                if (prev.right == null)
                {
                    prev.right = current;
                    current = current.left;
                }
                else
                {
                    prev.right = null;
                    answer.add(current.val);
                    current = current.right;
                }
            }
        }
        return answer;
    }
}