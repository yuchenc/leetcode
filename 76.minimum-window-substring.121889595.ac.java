class Solution {
    public String minWindow(String s, String t) {
        int[] array = new int[128];
        int count = t.length();
        int begin = 0;
        int end = 0;
        int min = Integer.MAX_VALUE;
        int head = 0;
        for (int i = 0; i < t.length(); i++)
            array[t.charAt(i)]++;
        while (end < s.length())
        {
            if (array[s.charAt(end)] > 0)
                count--;
            array[s.charAt(end)]--;
            end++;
            while (count == 0)
            {
                if (end - begin < min)
                {
                    min = end - begin;
                    head = begin;
                }
                if(array[s.charAt(begin)] == 0)
                    count++;
                array[s.charAt(begin)]++;
                begin++;
            }   
        }
        return (min == Integer.MAX_VALUE ? "" : s.substring(head, head + min));
    }
}