class Solution {
    public int combinationSum4(int[] nums, int target) {
        int answer[] = new int[target + 1];
        for (int i = 1; i <= target; i++)
        {
            for (int j = 0; j < nums.length; j++)
            {
                if (i > nums[j])
                    answer[i] += answer[i - nums[j]];
                else if (i == nums[j])
                    answer[i] += 1;
            }
        }
        return answer[target];
    }
}