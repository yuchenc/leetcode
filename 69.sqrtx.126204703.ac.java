class Solution {
    public int mySqrt(int x) {
        if (x == 0)
            return 0;
        int left = 1;
        int right = x;
        while (left <= right)
        {
            int middle = (left + right) / 2;
            if (middle > x/middle)
                right = middle - 1;
            else
            {
                if (middle + 1 > x/(middle+1))
                    return middle;
                left = middle + 1;
            }
        }
        return left;
    }
}