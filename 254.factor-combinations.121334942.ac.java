class Solution {
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> answer = new ArrayList<>();
        for (int i = 2; i <= Math.sqrt(n); i++)
        {
            if (n % i == 0)
            {
                answer.add(new ArrayList<Integer>(Arrays.asList(i, n / i)));
                List<List<Integer>> parent = getFactors(n / i);
                for (int j = 0; j < parent.size(); j++)
                {
                    List<Integer> tmp = parent.get(j);
                    if (tmp.get(0) >= i)
                    {
                        tmp.add(0, i);
                        answer.add(tmp);
                    }
                }
            }
        }
        return answer;
    }
}