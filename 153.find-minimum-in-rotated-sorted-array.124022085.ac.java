class Solution {
    public int findMin(int[] nums) {
        if (nums.length == 0)
            return 0;
        int first = nums[0];
        int left = 1;
        int right = nums.length - 1;
        int last = 0;
        while (left <= right)
        {
            int middle = (left + right) / 2;
            if (nums[middle] < first)
            {
                right = middle - 1;
                last = middle;
            }
            else
                left = middle + 1;
            
        }
        return nums[last];
    }
}