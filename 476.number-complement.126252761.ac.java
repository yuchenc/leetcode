class Solution {
    public int findComplement(int num) {
        int n = num;
        int count = 0;
        while (n != 0)
        {
            count++;
            n = n >> 1;
        }
        int mask = -1;
        mask = mask >>> (32 - count);
        return mask - num;
    }
}