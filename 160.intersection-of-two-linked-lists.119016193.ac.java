/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        int lengthA = findLength(headA);
        int lengthB = findLength(headB);
        if (lengthA > lengthB)
        {
            int extra = lengthA - lengthB;
            for (int i = 0; i < extra; i++)
                headA = headA.next;
        }
        else
        {
            int extra = lengthB - lengthA;
            for (int i = 0; i < extra; i++)
                headB = headB.next;
        }
        while (headA != null && headB != null && headA != headB)
        {
            headA = headA.next;
            headB = headB.next;
        }
        if (headA == null || headB == null)
            return null;
        return headA;
    }
    
    public int findLength(ListNode head)
    {
        int count = 0;
        while (head != null)
        {
            count++;
            head = head.next;
        }
        return count;
    }
}