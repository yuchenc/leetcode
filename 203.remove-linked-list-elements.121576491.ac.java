/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode removeElements(ListNode head, int val) {
        if (head == null)
            return null;
        while (head != null && head.val == val)
            head = head.next;
        if (head == null)
            return null;
        ListNode answer = new ListNode(head.val);
        ListNode tail = answer;
        ListNode current = head.next;
        while (current != null)
        {
            if (current.val != val)
            {
                tail.next = new ListNode(current.val);
                tail = tail.next;
            }
            current = current.next;
        }
        return answer;
    }
}