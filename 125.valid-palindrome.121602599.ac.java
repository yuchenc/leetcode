class Solution {
    public boolean isPalindrome(String s) {
        int left = 0;
        int right = s.length() - 1;
        while (left < right)
        {
            int c1 = s.charAt(left);
            int c2 = s.charAt(right);
            if (c1 >= 'A' && c1 <= 'Z')
                c1 = c1 - 'A' + 'a';
            if (c2 >= 'A' && c2 <= 'Z')
                c2 = c2 - 'A' + 'a';
            if (!(c1 >= 'a' && c1 <= 'z') && !(c1 >= '0' && c1 <= '9'))
            {
                left++;
                continue;
            }
            if (!(c2 >= 'a' && c2 <= 'z') && !(c2 >= '0' && c2 <= '9'))
            {
                right--;
                continue;
            }
            if (c1 != c2)
                return false;
            if (c1 == c2)
            {
                left++;
                right--;
            }
        }
        return true;
    }
}