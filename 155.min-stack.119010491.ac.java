class MinStack {
    Stack<Integer> value;
    Stack<Integer> min;
    
    /** initialize your data structure here. */
    public MinStack() {
        value = new Stack<>();
        min = new Stack<>();
    }
    
    public void push(int x) {
        value.push(x);
        if (min.empty())
            min.push(x);
        else
        {
            if (x < min.peek())
                min.push(x);
            else
                min.push(min.peek());
        }
        
    }
    
    public void pop() {
        value.pop();
        min.pop();
    }
    
    public int top() {
        return value.peek();
    }
    
    public int getMin() {
        return min.peek();
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */