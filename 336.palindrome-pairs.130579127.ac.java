class Solution {
    private class TrieNode {
        TrieNode[] children;
        int index;
        List<Integer> list;
        
        TrieNode() {
            this.children = new TrieNode[26];
            this.index = -1;
            this.list = new ArrayList<>();
        }
    }
    
    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> res = new ArrayList<>();
        if (words.length < 2)
            return res;
        TrieNode root = new TrieNode();
        for (int i = 0; i < words.length; i++)
            addWord(root, words[i], i);
        for (int i = 0; i < words.length; i++)
            search(root, res, words, i);
        return res;
    }
    
    private void search(TrieNode root, List<List<Integer>> res, String[] words, int i) {
        for (int j = 0; j < words[i].length(); j++)
        {	
    	    if (root.index >= 0 && root.index != i && isPalindrome(words[i], j, words[i].length() - 1))
    	        res.add(Arrays.asList(i, root.index));	
    	    root = root.children[words[i].charAt(j) - 'a'];
      	    if (root == null)
                return;
        }
    	
        for (int j : root.list) {
    	    if (i == j)
                continue;
    	    res.add(Arrays.asList(i, j));
        }
        return;
    }
    
    private void addWord (TrieNode root, String word, int index) {
        for (int i = word.length() - 1; i >= 0; i--)
        {
            int j = word.charAt(i) - 'a';
            if (root.children[j] == null)
                root.children[j] = new TrieNode();
            if (isPalindrome(word, 0, i))
                root.list.add(index);
            root = root.children[j];
        }
        root.list.add(index);
        root.index = index;
        return;
    }
    
    private boolean isPalindrome(String str, int i, int j) {
        while (i < j)
            if (str.charAt(i++) != str.charAt(j--))
                return false;
        return true;
    }
}