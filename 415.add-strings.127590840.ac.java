class Solution {
    public String addStrings(String num1, String num2) {
        if (num2.length() < num1.length())
            return addStrings(num2, num1);
        StringBuilder sb = new StringBuilder();
        int flag = 0;
        for (int i = num1.length() - 1; i >= 0; i--)
        {
            int a = num1.charAt(i) - '0';
            int b = num2.charAt(num2.length() + i - num1.length()) - '0';
            int c = (a + b + flag) % 10;
            if (a + b + flag >= 10)
                flag = 1;
            else
                flag = 0;
            char d = (char)(c + '0');
            sb.append(d);
        }
        for (int i = num2.length() - num1.length() - 1; i >= 0; i--)
        {
            int a = num2.charAt(i) - '0';
            int b = (a + flag) % 10;
            if (a + flag >= 10)
                flag = 1;
            else
                flag = 0;
            char c = (char)(b + '0');
            sb.append(c);
        }
        if (flag == 1)
            sb.append('1');
        return sb.reverse().toString();
    }
}