class Solution {
    public String convert(String s, int numRows) {
        String answer = "";
        int len = s.length();
        if (numRows == 1)
            return s;
        for(int i = 0; i < numRows; i++)
        {
            int step1 = (numRows - i - 1)*2;
            int step2 = i*2;
            int pos = i;
            if (pos < len)
                answer += s.charAt(pos);
            while (true)
            {
                if (step1 != 0)
                {
                    pos += step1;
                    if (pos < len)
                        answer += s.charAt(pos);
                    else
                        break;
                }
                if (step2 != 0)
                {
                    pos += step2;
                    if (pos < len)
                        answer += s.charAt(pos);
                    else
                        break;   
                }
            }
        }
        return answer;
    }
}