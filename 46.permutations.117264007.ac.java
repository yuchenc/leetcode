class Solution {
    public List<List<Integer>> permute(int[] nums) {
        //Collections.swap(List<?> list, int i, int j);
        List<List<Integer>> answer = new ArrayList<>();
        List<Integer> current = new ArrayList<>();
        for (int i = 0; i < nums.length; i++)
            current.add(nums[i]);
        answer.add(new ArrayList<Integer>(current));
        for (int i = 1; i < nums.length; i++)
        {
            Collections.swap(current, 0, i);
            answer.add(new ArrayList<Integer>(current));
        }
        //return answer;
        return getResult(answer, 1, nums.length);
    }
    
    public List<List<Integer>> getResult(List<List<Integer>> answer, int start, int end)
    {
        
        if (start == end)
            return answer;
        List<List<Integer>> newanswer = new ArrayList<>();
        for (int i = 0; i < answer.size(); i++)
        {
            List<Integer> current = answer.get(i);
            newanswer.add(new ArrayList<Integer>(current));
            for (int j = start + 1; j < end; j++)
            {
                Collections.swap(current, start, j);
                newanswer.add(new ArrayList<Integer>(current));
            }
        }
        return getResult(newanswer, start + 1, end);
    }
    
}