/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int kthSmallest(TreeNode root, int k) {
        List<Integer> list = new ArrayList<>();
        LMR(root, list, k);
        return list.get(k-1);
    }
    
    public boolean LMR(TreeNode root, List<Integer> list, int k) {
        if (root == null)
            return false;
        boolean left = LMR(root.left, list, k);
        if (left)
            return true;
        list.add(root.val);
        if (list.size() == k)
            return true;
        boolean right = LMR(root.right, list, k);
        if (right)
            return true;
        return false;
    }
}