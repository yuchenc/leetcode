class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        vector<vector<int>> answer;
        answer.clear();
        sort(nums.begin(), nums.end());
        if (nums.size() < 3)
            return answer;
        for (int i = 0; i < nums.size() - 2; i++)
        {
            int left = i + 1;
            int right = nums.size() - 1;
            if (i > 0 && nums.at(i) == nums.at(i - 1))
                continue;
            while (left != right)
            {
                if (left > i + 1 && nums.at(left) == nums.at(left - 1))
                {
                    left++;
                    continue;
                }
                if (nums.at(i) + nums.at(left) + nums.at(right) < 0)
                    left++;
                else if (nums.at(i) + nums.at(left) + nums.at(right) > 0)
                    right--;
                else
                //if (nums.at(i) + nums.at(left) + nums.at(right) == 0)
                {
                    vector<int> result;
                    result.clear();
                    result.push_back(nums.at(i));
                    result.push_back(nums.at(left));
                    result.push_back(nums.at(right));
                    answer.push_back(result);
                    left++;
                }
            }
        }
        return answer;
        
    }
};