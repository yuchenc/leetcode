class Solution {
    public int countBinarySubstrings(String s) {
        if (s.length() == 0)
            return 0;
        int answer = 0;
        char c = s.charAt(0);
        int left = 0;
        int right = 0;
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == c)
            {
                left++;
            }
            else
            {
                answer += Math.min(left, right);
                right = left;
                left = 1;
                c = s.charAt(i);
            }
        }
        return answer += Math.min(left, right);
    }
}
