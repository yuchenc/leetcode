/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    int max = 0;
    public int longestConsecutive(TreeNode root) {
        if (root == null)
            return 0;
        longestPath(root);
        return max;
    }
    
    public int longestPath(TreeNode root) {
        if (root == null)
            return 0;
        int left = 0;
        int right = 0;
        int res1 = longestPath(root.left);
        int res2 = longestPath(root.right);
        if (root.left != null && root.left.val == root.val + 1)
            left = res1;
        if (root.right != null && root.right.val == root.val + 1)
            right = res2;
        int res = Math.max(left, right) + 1;
        max = Math.max(max, res);
        return res;
    }
}