/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* answer = new ListNode(0);
        ListNode* current = answer;
        int flag = 0;
        int x;
        int y;
        while (l1 != NULL || l2 != NULL)
        {
            if (l1 == NULL)
                x = 0;
            else 
                x = l1->val;
            if (l2 == NULL)
                y = 0;
            else
                y = l2->val;
            int sum = flag + x + y;
            if (sum >= 10)
                flag = 1;
            else
                flag = 0;
            current->next = new ListNode(sum%10);
            current = current->next;
            if (l1 != NULL)
                l1 = l1->next;
            if (l2 != NULL)
                l2 = l2->next;
            
        }
        if (flag == 1)
            current->next = new ListNode(1);
        return answer->next;
        
    }
};