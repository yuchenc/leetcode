class Solution {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> answer = new ArrayList<>();
        List<Integer> current = new ArrayList<>();
        getResult(answer, current, 0, candidates, target);
        return answer;
    }
    
    public void getResult(List<List<Integer>> answer, List<Integer> current, int start, int[] candidates, int target)
    {
        if (target > 0)
        {
            for (int i = start; i < candidates.length && target >= candidates[i]; i++)
            {
                current.add(candidates[i]);
                getResult(answer, current, i, candidates, target - candidates[i]);
                current.remove(current.size() - 1);
            }
        }
        else if (target == 0)
            answer.add(new ArrayList<Integer>(current)); // Important, clone current
    }
}