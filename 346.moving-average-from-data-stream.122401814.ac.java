class MovingAverage {

    Queue<Integer> queue;
    int maxsize;
    int sum;
    /** Initialize your data structure here. */
    public MovingAverage(int size) {
        queue = new LinkedList<Integer>();
        maxsize = size;
        sum = 0;
    }
    
    public double next(int val) {
        if (queue.size() < maxsize)
        {
            queue.offer(val);
            sum += val;
            return sum * 1.0 / queue.size();
        }
        else
        {
            queue.offer(val);
            sum += val;
            sum -= queue.poll();
            return sum * 1.0 / queue.size();
        }
    }
}

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage obj = new MovingAverage(size);
 * double param_1 = obj.next(val);
 */