public class Solution {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int answer = 0;
        while (n != 0)
        {
            answer += n & 1;
            n = n >>> 1;
        }
        return answer;
    }
}