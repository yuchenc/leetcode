# Write your MySQL query statement below
select Trips.Request_at 'Day',
       round(sum(if(status != 'completed', 1, 0)) / sum(1), 2) 'Cancellation Rate'
from Trips
Join Users
On Trips.Client_Id = Users.Users_Id
Where Users.Banned = 'No'
and Trips.Request_at between '2013-10-01' AND '2013-10-03'
GROUP BY Trips.Request_at