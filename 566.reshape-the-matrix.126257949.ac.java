class Solution {
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        if (nums.length == 0 || nums[0].length == 0)
            return nums;
        int m = nums.length;
        int n = nums[0].length;
        if (r * c > m * n)
            return nums;
        int max = r * c;
        int[][] res = new int[r][c];
        int index = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (index > max)
                    break;
                res[index/c][index%c] = nums[i][j];
                index++;
            }
        }
        return res;
    }
}