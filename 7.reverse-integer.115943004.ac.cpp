class Solution {
public:
    int reverse(int x) {
        int q = x % 10;
        int p = x / 10;
        int answer = q;
        int newanswer = answer;
        while (p != 0)
        {
            q = p % 10;
            p = p / 10;
            newanswer = answer * 10 + q;
            if (newanswer/10 != answer)
                return 0;
            answer = newanswer;
        }
        
        return answer;
        
    }
};