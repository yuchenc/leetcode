class Solution {
    public boolean hasAlternatingBits(int n) {
        if (n == 0)
            return true;
        long start = 1;
        while (start <= n)
        {
            if (n == start)
                return true;
            if (start % 2 == 1)
                start = 2 * start;
            else
                start = 2 * start + 1;
        }
        return false;
    }
}