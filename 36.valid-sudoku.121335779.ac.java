class Solution {
    public boolean isValidSudoku(char[][] board) {
        int n = board.length;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (board[i][j] != '.')
                    if (!checkValid(board, i, j))
                        return false;
        return true;
    }
    public boolean checkValid(char[][] board, int x, int y)
    {
        int n = board.length;
        char c = board[x][y];
        for (int j = y + 1; j < n; j++)
            if (board[x][j] == c)
                return false;
        for (int i = x + 1; i < n; i++)
            if (board[i][y] == c)
                return false;
        int xIndex = x / 3;
        int yIndex = y / 3;
        for (int i = xIndex * 3; i < xIndex * 3 + 3; i++)
            for (int j = yIndex * 3; j < yIndex * 3 + 3; j++)
            {
                if (i == x && j == y)
                    continue;
                else
                    if (board[i][j] == c)
                        return false;
            }
        return true;
        
    }
}