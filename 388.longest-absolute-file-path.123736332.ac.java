class Solution {
    public int lengthLongestPath(String input) {
        Stack<String> stack = new Stack<>();
        int max = 0;
        int sum = 0;
        String tmp = "";
        int level = 0;
        for (int i = 0; i < input.length(); i++)
        {
            if (input.charAt(i) != '\n' && input.charAt(i) != '\t')
                tmp += input.charAt(i);
            else if (input.charAt(i) == '\n')
            {
                int size = stack.size();
                if (level < size)
                    for (int j = level; j < size; j++)
                        sum -= stack.pop().length();
                sum += tmp.length();
                stack.push(tmp);
                if (tmp.indexOf(".") > 0)
                    max = Math.max(max, sum + stack.size() - 1);
                tmp = "";
                level = 0;
            }
            else if (input.charAt(i) == '\t')
                level++;
        }
        int size = stack.size();
        if (level < size)
            for (int j = level; j < size; j++)
                sum -= stack.pop().length();
        sum += tmp.length();
        if (tmp.indexOf(".") > 0)
            max = Math.max(max, sum + stack.size());
        return max;
    }
}