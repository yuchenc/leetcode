class Solution {
    public int uniquePaths(int m, int n) {
        int min = Math.min(m - 1, n - 1);
        int total = m + n - 2;
        double answer = 1;
        for (int i = 0; i < min; i++)
        {
            answer = answer * (total - i) / (i + 1);
            //System.out.println(answer);
        }
        return (int)answer;
    }
}