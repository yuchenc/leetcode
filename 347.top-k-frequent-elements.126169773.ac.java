class Solution {
    public List<Integer> topKFrequent(int[] nums, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int num : nums)
        {
            if (map.containsKey(num))
                map.put(num, map.get(num) + 1);
            else
                map.put(num, 1);
        }
        List<Integer>[] list = new List[nums.length + 1];
        List<Integer> answer = new ArrayList<>();
        for (int key : map.keySet())
        {
            int frequency = map.get(key);
            if (list[frequency] == null)
                list[frequency] = new ArrayList<Integer>();
            list[frequency].add(key);
        }
        for (int i = nums.length; i >= 0 && answer.size() < k; i--)
        {
            if (list[i] != null)
            {
                for (int j = 0; j < list[i].size() && answer.size() < k; j++)
                    answer.add(list[i].get(j));
            }
        }
        return answer;
    }
}