class Solution {
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> answer = new ArrayList<>();
        if (matrix.length == 0 || matrix[0].length == 0)
            return answer;
        spiralOrder(matrix, 0, answer);
        return answer;
    }
    
    public void spiralOrder(int[][] matrix, int level, List<Integer> answer)
    {
        int m = matrix.length;
        int n = matrix[0].length;
        if (level >= Math.min(m+1, n+1) / 2)
            return;
        else
        {
            int i = level;
            int j = level;
            for (j = level; j < n - level; j++)
                answer.add(matrix[i][j]);
            j--;
            for (i = i + 1; i < m - level; i++)
                answer.add(matrix[i][j]);
            i--;
            if (level + 1 != Math.min(m - level, n - level))
            {
                for (j = j - 1; j >= level; j--)
                    answer.add(matrix[i][j]);
                j++;
                for (i = i - 1; i > level; i--)
                    answer.add(matrix[i][j]);
            }
            spiralOrder(matrix, level + 1, answer);
        }
    }
}