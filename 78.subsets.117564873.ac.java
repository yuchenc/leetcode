class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> answer = new ArrayList<>();
        answer.add(new ArrayList<Integer>());
        for (int i : nums)
        {
            List<List<Integer>> tmp = new ArrayList<>();
            for (List<Integer> sub : answer)
            {
                List<Integer> one = new ArrayList<>(sub);
                one.add(i);
                tmp.add(one);
            }
            answer.addAll(tmp);
        }
        return answer;   
    }
}