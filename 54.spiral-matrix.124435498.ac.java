class Solution {
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> answer = new ArrayList<>();
        if (matrix.length == 0 || matrix[0].length == 0)
            return answer;
        int m = matrix.length;
        int n = matrix[0].length;
        for (int level = 0; level < (Math.min(m,n)+1)/2; level++)
        {
            for (int i = level; i < n - level; i++)
                answer.add(matrix[level][i]);
            for (int i = level + 1; i < m - level; i++)
                answer.add(matrix[i][n-level-1]);
            if (level + 1 >= Math.min(m,n) - level)
                continue;
            for (int i = n - level - 2; i >= level; i--)
                answer.add(matrix[m-level-1][i]);
            for (int i = m - level - 2; i > level; i--)
                answer.add(matrix[i][level]);
        }
        return answer;   
    }
}