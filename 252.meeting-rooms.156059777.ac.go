type sortable []Interval
func (s sortable) Len() int { return len(s) }
func (s sortable) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s sortable) Less(i, j int) bool {
    a, b := s[i], s[j]
    return a.Start < b.Start
}
func canAttendMeetings(intervals []Interval) bool {
    var s = sortable(intervals)
    sort.Sort(s)
    for i := 1; i < len(s); i++ {
        if s[i].Start < s[i-1].End {
            return false
        }
    }
    return true
}