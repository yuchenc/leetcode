class Solution {
    public int arrayNesting(int[] nums) {
        int answer = 0;
        int[] visited = new int[nums.length];
        for (int i = 0; i < nums.length; i++)
        {
            if (visited[i] == 1)
                continue;
            else
            {
                int length = 0;
                int j = nums[i];
                while (visited[j] == 0)
                {
                    visited[j] = 1;
                    j = nums[j];
                    length++;
                }
                answer = Math.max(answer, length);
            }
        }
        return answer;
    }
}