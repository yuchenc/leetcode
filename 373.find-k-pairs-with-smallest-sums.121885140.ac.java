class Solution {
    public List<int[]> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        PriorityQueue<int[]> que = new PriorityQueue<>((a,b)->a[0]+a[1]-b[0]-b[1]);
        List<int[]> answer = new ArrayList<>();
        int m = nums1.length;
        int n = nums2.length;
        if (m == 0 || n == 0)
            return answer;
        if (k == 0)
            return answer;
        for (int i = 0; i < Math.min(m, k); i++)
            que.offer(new int[]{nums1[i], nums2[0], 0});
        while (k > 0 && !que.isEmpty())
        {
            int[] cur = que.poll();
            answer.add(new int[]{cur[0], cur[1]});
            k--;
            if (cur[2] == n-1)
                continue;
            que.offer(new int[]{cur[0], nums2[cur[2]+1], cur[2]+1});
        }
        return answer;
    }
}