class Solution {
    public int maxCount(int m, int n, int[][] ops) {
        int mina = m;
        int minb = n;
        for (int i = 0; i < ops.length; i++)
        {
            mina = Math.min(mina, ops[i][0]);
            minb = Math.min(minb, ops[i][1]);
        }
        return mina * minb;
    }
}