class Solution {
    public int minMoves(int[] nums) {
        if (nums.length <= 1)
            return 0;
        int min = nums[0];
        int answer = 0;
        for (int i = 1; i < nums.length; i++)
        {
            if (nums[i] >= min)
                answer += nums[i] - min;
            else
            {
                answer += i * (min - nums[i]);
                min = nums[i];
            }
        }
        return answer;
    }
}