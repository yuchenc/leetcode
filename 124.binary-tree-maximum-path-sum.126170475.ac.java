/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    int max = Integer.MIN_VALUE;
    public int maxPathSum(TreeNode root) {
        search(root);
        return max;
    }
    
    public int search(TreeNode root) {
        if (root == null)
            return 0;
        int left = search(root.left);
        int right = search(root.right);
        int res = 0;
        if (left < 0 && right < 0)
            res = root.val;
        else if (left < 0 && right >= 0)
            res = root.val + right;
        else if (left >= 0 && right < 0)
            res = root.val + left;
        else
        {
            res = Math.max(left, right) + root.val;
            max = Math.max(left + right + root.val, max);
        }
        max = Math.max(res, max);
        return res;
    }
}