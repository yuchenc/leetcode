class Solution {
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> answer = new ArrayList<>();
        List<Integer> big = new ArrayList<>();
        List<Integer> small = new ArrayList<>();
        List<Integer> equal = new ArrayList<>();
        if (nums.length == 0)
            return answer;
        int num = nums[0];
        for (int i = 1; i < nums.length; i++)
        {
            if (nums[i] == num)
                equal.add(nums[i]);
            else if (nums[i] < num)
                small.add(nums[i]);
            else
                big.add(nums[i]);
        }
        answer.add(small.size());
        for (int i = 1; i < nums.length; i++)
        {
            if (nums[i] == nums[i-1])
            {
                answer.add(answer.get(answer.size() - 1));
                equal.remove(equal.size() - 1);
            }
            else if (nums[i] > nums[i-1])
            {
                small.addAll(equal);
                equal = new ArrayList<>();
                List<Integer> tmp = new ArrayList<>();
                for (int j = 0; j < big.size(); j++)
                {
                    if (big.get(j) < nums[i])
                        small.add(big.get(j));
                    else if (big.get(j) == nums[i])
                        equal.add(big.get(j));
                    else
                        tmp.add(big.get(j));
                }
                equal.remove(equal.size() - 1);
                big = tmp;
                answer.add(small.size());
            }
            else
            {
                big.addAll(equal);
                equal = new ArrayList<>();
                List<Integer> tmp = new ArrayList<>();
                for (int j = 0; j < small.size(); j++)
                {
                    if (small.get(j) < nums[i])
                        tmp.add(small.get(j));
                    else if (small.get(j) == nums[i])
                        equal.add(small.get(j));
                    else
                        big.add(small.get(j));
                }
                equal.remove(equal.size() - 1);
                small = tmp;
                answer.add(small.size());
            }
        }
        return answer;
    }
}