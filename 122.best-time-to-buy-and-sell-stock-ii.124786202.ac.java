class Solution {
    public int maxProfit(int[] prices) {
        if (prices.length <= 1)
            return 0;
        int answer = 0;
        for (int i = 1; i < prices.length; i++)
            answer += (prices[i] > prices[i-1] ? prices[i] - prices[i-1] : 0);
        return answer;
        
    }
}