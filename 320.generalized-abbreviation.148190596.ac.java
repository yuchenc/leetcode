class Solution {
    public List<String> generateAbbreviations(String word) {
        List<String> res = new ArrayList<>();
        if (word.length() == 0)
        {
            res.add(word);
            return res;
        }
        else if (word.length() == 1)
        {
            res.add(word);
            res.add("1");
            return res;
        }
        List<String> rec = generateAbbreviations(word.substring(1));
        for (String str : rec) {
            if (str.charAt(0) >= '1' && str.charAt(0) <= '9') {
                int index = 1;
                while (index < str.length() && str.charAt(index) >= '0' && str.charAt(index) <= '9') {
                    index++;
                }
                int count = Integer.parseInt(str.substring(0, index));
                count++;
                res.add(String.valueOf(count) + str.substring(index));
                res.add(word.charAt(0) + str);
            }
            else {
                res.add(word.charAt(0) + str);
                res.add("1" + str);
            }
        }
        return res;
    }
}