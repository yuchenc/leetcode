class MinStack {
    long min;
    Stack<Long> value;
    
    /** initialize your data structure here. */
    public MinStack() {
        value = new Stack<>();
    }
    
    public void push(int x) {
        if (value.empty())
        {
            value.push(0L);
            min = x;
        }
        else
        {
            value.push(x - min);
            if (x < min)
                min = x;
        }
    }
    
    public void pop() {
        if (value.empty())
            return;
        long pop = value.pop();
        if (pop < 0)
            min = min - pop;
    }
    
    public int top() {
        long top = value.peek();
        if (top > 0)
            return (int)(top + min);
        else
            return (int)min;
    }
    
    public int getMin() {
        return (int)min;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */