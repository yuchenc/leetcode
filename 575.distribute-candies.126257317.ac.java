class Solution {
    public int distributeCandies(int[] candies) {
        HashSet<Integer> set = new HashSet<>();
        for (int num : candies)
            set.add(num);
        if (set.size() >= candies.length/2)
            return candies.length/2;
        else
            return set.size();   
    }
}