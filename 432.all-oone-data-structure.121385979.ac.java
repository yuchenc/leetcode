class AllOne {
    class valueNode {
        valueNode pre;
        valueNode next;
        int value;
        List<String> keys;
        
        valueNode (int value, String key)
        {
            this.value = value;
            keys = new LinkedList<String>();
            keys.add(key);
        }
    }
    
    private valueNode head;
    private valueNode tail;
    private valueNode curNode;
    private HashMap<String, valueNode> keyMap;

    /** Initialize your data structure here. */
    public AllOne() {
        keyMap = new HashMap<String,valueNode>();
        head = null;
        tail = null;
    }
    
    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if (tail == null) { // which means head must be null too and the keyMap must be empty.
                curNode = new valueNode(1,key);
                head = curNode;
                tail = curNode;
                keyMap.put(key,curNode);
        } else if (!keyMap.containsKey(key)){ //which means that this key should be add to the tail.
                if (tail.value == 1) {//which means just add the key to the tail list.
                    tail.keys.add(key);
                    keyMap.put(key,tail);
                }else {    //which means have to add a value= 1 node.
                    curNode = new valueNode(1,key);
                    curNode.pre = tail;
                    tail.next = curNode;
                    tail = curNode;
                    keyMap.put(key,curNode);
                }
        } else { //which the string already exists.
            curNode = keyMap.get(key);
            if (curNode.pre != null) {  //which means the node is in the middle.
                if (curNode.pre.value == curNode.value + 1){
                    curNode.pre.keys.add(key);
                    curNode.keys.remove(key); 
                    keyMap.put(key,curNode.pre);
                    checkEmpty(curNode);
                }else {                     //which means the preNode value != curNode.value;
                    valueNode newNode = new valueNode(curNode.value+1, key);
                    newNode.pre = curNode.pre;
                    newNode.next= curNode;
                    newNode.pre.next = newNode;
                    curNode.pre = newNode;
                    curNode.keys.remove(key); 
                    keyMap.put(key, newNode);
                    checkEmpty(curNode);
                }
            }else {//which means the node is the head. so we build a new head.
                head = new valueNode(curNode.value+1,key);
                head.next = curNode;
                curNode.pre = head;
                curNode.keys.remove(key);
                keyMap.put(key, head);
                checkEmpty(curNode);
            }
        }
    }
    
    private void checkEmpty(valueNode checkNode) {
            if (checkNode.keys.size() != 0) return;
            if (checkNode.pre == null && checkNode.next == null){
                tail = null;
                head = null;
            }else if (checkNode.pre == null && checkNode.next != null) {
                head = checkNode.next;
                head.pre = null;
            }else if (checkNode.next == null && checkNode.pre != null){
                tail = checkNode.pre;
                tail.next = null;
            }else {
                checkNode.pre.next = checkNode.next;
                checkNode.next.pre  = checkNode.pre; 
            } 
    }
    
    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if (!keyMap.containsKey(key)) // head == null
            return; //which means nothing here.
                    //or  means no key in the structrue.
        curNode = keyMap.get(key);
        if (curNode.next != null) 
        {  //which means the node is in the middle.
            if (curNode.next.value == curNode.value - 1)
            { //which means we can just 
                curNode.next.keys.add(key);
                curNode.keys.remove(key); 
                keyMap.put(key,curNode.next);
                checkEmpty(curNode);
            }else
            {                     //which means the nextNode value != curNode.value-1;
                valueNode newNode = new valueNode(curNode.value-1, key);
                newNode.next = curNode.next;
                newNode.pre = curNode;
                newNode.next.pre = newNode;
                curNode.keys.remove(key); 
                curNode.next = newNode;
                keyMap.put(key,newNode);
                checkEmpty(curNode);
            }
        }
        else
        {    //which means the node is the tail. so we build a new head.
            if (curNode.value == 1)
            {     //just to delete the key.
                curNode.keys.remove(key);  
                keyMap.remove(key);
                checkEmpty(curNode);
            }
            else
            {                         // build another tail.
                tail = new valueNode(curNode.value-1,key);
                tail.pre = curNode;
                curNode.next = tail;
                curNode.keys.remove(key);  
                keyMap.put(key, tail);
                checkEmpty(curNode);
            }
        }
    }
    
    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        if (head == null)
            return "";
        else
            return head.keys.get(0);
    }
    
    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
        if (tail == null)
            return "";
        else
            return tail.keys.get(0);
    }
}

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne obj = new AllOne();
 * obj.inc(key);
 * obj.dec(key);
 * String param_3 = obj.getMaxKey();
 * String param_4 = obj.getMinKey();
 */