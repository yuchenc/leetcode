class AllOne {
    class node {
        int val;
        node prev;
        node next;
        List<String> keys;
        node (int value, String key)
        {
            this.val = value;
            this.keys = new LinkedList<String>();
            keys.add(key);
        }
    }
    
    private HashMap<String, node> map;
    private node head;
    private node tail;

    /** Initialize your data structure here. */
    public AllOne() {
        map = new HashMap<>();
        head = new node(-1, "head");
        tail = new node(-1, "tail");
        head.next = tail;
        tail.prev = head;
    }
    
    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if (map.containsKey(key))
        {
            node current = map.get(key);
            if (current.prev.val == current.val + 1)
            {
                current.prev.keys.add(key);
                current.keys.remove(key);
                map.put(key, current.prev);
                check(current);
            }
            else
            {
                node newnode = new node(current.val + 1, key);
                newnode.prev = current.prev;
                current.prev.next = newnode;
                newnode.next = current;
                current.prev = newnode;
                current.keys.remove(key);
                map.put(key, newnode);
                check(current);
            }
        }
        else
        {
            if (tail.prev.val == 1)
            {
                tail.prev.keys.add(key);
                map.put(key, tail.prev);
            }
            else
            {
                node current = new node(1, key);
                current.prev = tail.prev;
                tail.prev.next = current;
                current.next = tail;
                tail.prev = current;
                map.put(key, current);
            }
        }
    }
    
    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if (map.containsKey(key))
        {
            node current = map.get(key);
            if (current.val == 1)
            {
                current.keys.remove(key);
                map.remove(key);
                check(current);
            }
            else
            {
                if (current.next.val == current.val - 1)
                {
                    current.next.keys.add(key);
                    current.keys.remove(key);
                    map.put(key, current.next);
                    check(current);
                }
                else
                {
                    node newnode = new node(current.val - 1, key);
                    newnode.next = current.next;
                    current.next.prev = newnode;
                    newnode.prev = current;
                    current.next = newnode;
                    current.keys.remove(key);
                    map.put(key, newnode);
                    check(current);
                }
            }
        }
    }
    
    public void check(node current) {
        if (current.keys.size() == 0)
        {
            current.prev.next = current.next;
            current.next.prev = current.prev;
        }
    }
    
    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        if (head.next != tail)
            return head.next.keys.get(0);
        else
            return "";
    }
    
    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
        if (tail.prev != head)
            return tail.prev.keys.get(0);
        else
            return "";
    }
}

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne obj = new AllOne();
 * obj.inc(key);
 * obj.dec(key);
 * String param_3 = obj.getMaxKey();
 * String param_4 = obj.getMinKey();
 */