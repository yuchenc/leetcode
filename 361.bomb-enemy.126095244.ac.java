class Solution {
    public int maxKilledEnemies(char[][] grid) {
        if (grid.length == 0 || grid[0].length == 0)
            return 0;
        int m = grid.length;
        int n = grid[0].length;
        int max = 0;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (!visited[i][j] && grid[i][j] == '0')
                {
                    int res = search(grid, visited, i, j);
                    max = Math.max(res, max);
                }
            }
        }
        return max;
    }
    
    public int search(char[][] grid, boolean[][] visited, int x, int y) {
        int m = grid.length;
        int n = grid[0].length;
        int res = 0;
        visited[x][y] = true;
        for (int i = x + 1; i < m; i++)
        {
            if (grid[i][y] == 'E')
                res++;
            else if (grid[i][y] == 'W')
                break;
        }
        for (int i = x - 1; i >= 0; i--)
        {
            if (grid[i][y] == 'E')
                res++;
            else if (grid[i][y] == 'W')
                break;
        }
        for (int i = y + 1; i < n; i++)
        {
            if (grid[x][i] == 'E')
                res++;
            else if (grid[x][i] == 'W')
                break;
        }
        for (int i = y - 1; i >= 0; i--)
        {
            if (grid[x][i] == 'E')
                res++;
            else if (grid[x][i] == 'W')
                break;
        }
        return res;
    }
}