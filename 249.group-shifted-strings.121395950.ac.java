class Solution {
    public List<List<String>> groupStrings(String[] strings) {
        List<List<String>> answer = new ArrayList<>();
        for (int i = 0; i < strings.length; i++)
        {
            String current = strings[i];
            int j = 0;
            for (; j < answer.size(); j++)
            {
                List<String> tmp = answer.get(j);
                String test = tmp.get(0);
                if (test.length() == current.length())
                    if (check(test, current))
                    {
                        tmp.add(current);
                        break;
                    }
            }
            if (j == answer.size())
            {
                List<String> newList = new ArrayList<>();
                newList.add(current);
                answer.add(newList);
            }
        }
        return answer;
    }
    
    public boolean check(String left, String right)
    {
        int c1 = (int)(left.charAt(0) - 'a');
        int c2 = (int)(right.charAt(0) - 'a');
        int diff = c2 - c1;
        for (int i = 1; i < left.length(); i++)
        {
            c1 = (int)(left.charAt(i) - 'a');
            c2 = (int)(right.charAt(i) - 'a');
            int shouldBe = c1 + diff;
            if (shouldBe < 0)
                shouldBe += 26;
            if (shouldBe >= 26)
                shouldBe -= 26;
            if (c2 != shouldBe)
                return false;
        }
        return true;
    }
}