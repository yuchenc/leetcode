class Solution {
    public boolean exist(char[][] board, String word) {
        boolean answer = false;
        int row = board.length;
        int col = board[0].length;
        int[][] status = new int[row][col];
        for (int i = 0; i < row; i++)
            for (int j = 0; j < col; j++)
                status[i][j] = 0;
        for (int i = 0; i < board.length; i++)
        {
            for (int j = 0; j < board[0].length; j++)
            {
                if (board[i][j] == word.charAt(0))
                {
                    status[i][j] = 1;
                    answer = search(board, word, i, j, 1, status, row, col);
                    if (answer == true)
                        return answer;
                }
            }
        }
        return answer;
    }
    
    public boolean search(char[][] board, String word, int i, int j, int count, int[][] status, int row, int col)
    {
        //System.out.print(count + " " + word.length() + "!");
        if (count == word.length())
            return true;
        boolean answer = false;
        boolean right = false;
        boolean up = false;
        boolean down = false;
        boolean left = false;
        //System.out.println(i + " " + j + " " + count + " " + word.charAt(count));
        if (i > 0)
        {
            if (status[i-1][j] == 0 && board[i-1][j] == word.charAt(count))
            {
                status[i-1][j] = 1;
                up = search(board, word, i-1, j, count+1, status, row, col);
                if (up == true)
                    return true;
            }
        }
        if (i < row - 1)
        {
            //System.out.println("count: ", count);
            //System.out.println("i: ", i);
            //System.out.println("j: ", j);
            if (status[i+1][j] == 0 && board[i+1][j] == word.charAt(count))
            {
                status[i+1][j] = 1;
                //System.out.println("count! : " + count);
                down = search(board, word, i+1, j, count+1, status, row, col);
                if (down == true)
                    return true;
            }
        }
        if (j > 0)
        {
            if (status[i][j-1] == 0 && board[i][j-1] == word.charAt(count))
            {
                status[i][j-1] = 1;
                left = search(board, word, i, j-1, count+1, status, row, col);
                if (left == true)
                    return true;
            }
        }
        if (j < col - 1)
        {
            if (status[i][j+1] == 0 && board[i][j+1] == word.charAt(count))
            {
                status[i][j+1] = 1;
                right = search(board, word, i, j+1, count+1, status, row, col);
                if (right == true)
                    return true;
            }
        }
        status[i][j] = 0;
        return false;
    }
}