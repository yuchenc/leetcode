class Solution {
    public int climbStairs(int n) {
        if (n == 1)
            return 1;
        int[] matrix = new int[4];
        int[] left = {1,1,1,0};
        matrix = matrixPow(left, n - 1);
        return matrix[0] + matrix[1]; 
    }
    
    public int[] matrixPow(int[] matrix, int k)
    {
        if (k == 1)
            return matrix;
        int[] middle = matrixPow(matrix, k / 2);
        int[] answer = matrixMul(middle, middle);
        if (k % 2 == 1)
        {
            answer = matrixMul(answer, matrix);
        }
        return answer;
    }
    
    public int[] matrixMul(int[] matrix1, int[] matrix2)
    {
        int[] result = new int[4];
        result[0] = matrix1[0] * matrix2[0] + matrix1[1] * matrix2[2];
        result[1] = matrix1[0] * matrix2[1] + matrix1[1] * matrix2[3];
        result[2] = matrix1[2] * matrix2[0] + matrix1[3] * matrix2[2];
        result[3] = matrix1[2] * matrix2[1] + matrix1[3] * matrix2[3];
        return result;
    }
}