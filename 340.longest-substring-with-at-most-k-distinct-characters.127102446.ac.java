class Solution {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        int[] map = new int[256];
        int left = 0;
        int right = 0;
        int ans = 0;
        int count = 0;
        for (; right < s.length(); right++)
        {
            char c = s.charAt(right);
            if (map[c] == 0)
                count++;
            map[c]++;
            while (count > k)
            {
                char p = s.charAt(left);
                if (map[p] == 1)
                    count--;
                map[p]--;
                left++;
            }
            ans = Math.max(ans, right - left + 1);
        }
        return ans;
    }
}