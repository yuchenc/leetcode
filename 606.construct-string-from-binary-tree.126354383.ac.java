/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public String tree2str(TreeNode t) {
        if (t == null)
            return "";
        String res = String.valueOf(t.val);
        if (t.left == null && t.right == null)
            return res;
        if (t.left != null && t.right == null)
        {
            res += "(" + tree2str(t.left) + ")";
            return res;
        }
        else
        {
            res += "(" + tree2str(t.left) + ")" + "(" + tree2str(t.right) + ")";
            return res;
        }
    }
}