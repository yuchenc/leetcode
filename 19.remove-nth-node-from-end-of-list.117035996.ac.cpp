/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode *prevhead = new ListNode(0);
        prevhead->next = head;
        ListNode *first = prevhead;
        ListNode *second = prevhead;
        for (int i = 0; i < n + 1; i++)
            first = first->next;
        while (first != NULL)
        {
            first = first->next;
            second = second->next;
        }
        second->next = second->next->next;
        return prevhead->next;
    }
};