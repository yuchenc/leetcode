/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public static ListNode mergeKLists(ListNode[] lists) {
        return binary(lists, 0, lists.length - 1);
    }
    
    public static ListNode binary(ListNode[] lists, int left, int right) {
        System.out.println(left + " " + right);
        if (left == right)
            return lists[left];
        if (left < right)
        {
            int middle = (left + right) / 2;
            ListNode l1 = binary(lists, left, middle);
            ListNode l2 = binary(lists, middle+1, right);
            return merge(l1, l2);
        }
        else
            return null;
    }
    
    public static ListNode merge(ListNode left, ListNode right) {
        if (left == null)
            return right;
        if (right == null)
            return left;
        if (left.val < right.val)
        {
            left.next = merge(left.next, right);
            return left;
        }
        else
        {
            right.next = merge(left, right.next);
            return right;
        }
    }
    
}