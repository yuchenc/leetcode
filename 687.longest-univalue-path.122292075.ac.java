/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    int max = 0;
    public int longestUnivaluePath(TreeNode root) {
        goThrough(root);
        return max;
    }
    
    public int goThrough(TreeNode root) {
        int res1 = 0;
        int res2 = 0;
        if (root == null)
            return 0;
        int left = goThrough(root.left);
        int right = goThrough(root.right);
        if (root.left != null && root.left.val == root.val)
            res1 += left + 1;
        if (root.right != null && root.right.val == root.val)
            res2 += right + 1;
        int res = Math.max(res1, res2);
        max = Math.max(max, res1 + res2);
        return res;
    }
        
}