class Solution {
    public boolean wordPattern(String pattern, String str) {
        String[] strs = str.split(" ");
        if (strs.length != pattern.length())
            return false;
        HashMap<Character, String> map1 = new HashMap<>();
        HashMap<String, Character> map2 = new HashMap<>();
        for (int i = 0; i < strs.length; i++)
        {
            if (map1.containsKey(pattern.charAt(i)) && map2.containsKey(strs[i]))
            {
                if (!map1.get(pattern.charAt(i)).equals(strs[i]) || map2.get(strs[i]) != pattern.charAt(i))
                    return false;
            }
            else if (!map1.containsKey(pattern.charAt(i)) && !map2.containsKey(strs[i]))
            {
                map1.put(pattern.charAt(i), strs[i]);
                map2.put(strs[i], pattern.charAt(i));
            }
            else
                return false;
        }
        return true;
    }
}